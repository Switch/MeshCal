<?php
header('Content-Type: text/html; charset=UTF-8');

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/* verouille cetaines fonctionalité pour facilité le dévelopement  */
/* à passer en "false" pour débloquer pour metre en prod           */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
$developement=true; 

//////////////////////////////////////////////////////////////////////////////
// Creation du tableau d'erreures toujours en premier avant tout traitement //
//////////////////////////////////////////////////////////////////////////////
$erreurs=array();

include "res/_config_.php"; // toujours en premier !!! (variable de configuration)
include "res/res_linguistique.php";
include "res/res_cal.php"; //resources specialement pour manipuler les fichier ICS locaux

	
////////////////////////////////////
// preparation des lien de retour //
////////////////////////////////////
$url_tab=parse_url($_SERVER['REQUEST_URI']);
/*echo "<pre>";
print_r ($_SERVER);
echo "</pre><htr>";*/
$query = isset($url_tab['query']) ? '?' . $url_tab['query'] : '';
if(!isset($url_tab['query']))$url_tab['query']="";	


///////////////////////////////////////////
// gestion de connexion et de session... //
///////////////////////////////////////////
$session=include("res/session_interface.php");


//////////////////////////////////////
// entre les balises "head" html... //
//////////////////////////////////////
$head=include("res/head.php");


////////////////////////////////////////////
// renseignement des $metadonees générale //
////////////////////////////////////////////
$tab=listage_metadonees_calendriers(); 
/*echo "<hr><pre>";
print_r($tab);
echo "</pre><br>";*/
$metadonees_calendriers=$tab["cal"]; //tout les calendrier...
$privilege=$tab["privileges"]; //les autorisation spécific à l'utilisateur calendrier par calendrier...
$nb_cal=$tab["nb"]; //nombre de calendrier (tataux, distant, local)
$traiter_cal=true; //afficher le calendrier ?


if(isset($_GET['aff'])){
	$tab_req=explode("-",$_GET['aff']);
}else{
	$tab_req=array("A", strftime("%Y")); /*** option par défaut à piloter... */
	$tab_req=array("M", strftime("%Y"), strftime("%m"));
}
//print_r($tab_req);
//echo "<br>";








if(($nb_cal["locaux"]==0) || (!isset($_SESSION['log']))){ //Monkey_date ne peut etre invoqué s'il n'y a pas de calendrier local et si l'utilisateur n'est pas identifier
	unset($_POST["Monkey_Date"]);/* a modifier pour permetre passerelle en demandant de s'identifier dans Monkey_Date */
}else{
	if($_SESSION['niv']>5 )unset($_POST["Monkey_Date"]); // interdiction au bannis !!!
	/** fair en sorte que les droit soit trié par calendrier... */
}

if(isset($_POST["Monkey_Date"])){
	$corps=include("res/monkey_date.php"); //Monkey... (formulaire de création/modification d'event
	$traiter_cal=false;
}


/* traitement de requette Monkey_Date */
if(isset($_POST["Monkey_Date_action"])){
	if($metadonees_calendriers[$_POST["cal_cible"]]["type"]=="local")if (droit_cal($_POST["cal_cible"])<3){	/* control si pas banni entretemps !!! */ /* a l'avenir, traité la diférence entre créer et modifier */ // control des droit
		/*echo "<hr><pre>";
		print_r($_POST);
		echo "</pre><br>";*/
		
		//echo "[".$_POST["cal_cible"]."]{".$rep_cal.$metadonees_calendriers[$_POST["cal_cible"]]["adresse"]."}<br>";
		$cal_cible=$rep_cal.$metadonees_calendriers[$_POST["cal_cible"]]["adresse"];
		if(isset($_POST["cal_base"]) && $metadonees_calendriers[$_POST["cal_base"]]["type"]=="local")$cal_base=$rep_cal.$metadonees_calendriers[$_POST["cal_base"]]["adresse"];
		switch ($_POST["Monkey_Date_action"]) {
			case "creer":
				insert_event(POST2event_ical(),$cal_cible);	
			break;
			case "deriver":
				insert_event(POST2event_ical(),$cal_cible);				
			break;		
			case "modifier":
				if ($_POST["cal_base"]==$_POST["cal_cible"]){ //si c'est tout dans me même calendrier
					remplace_event($cal_cible, $_POST["id_base"],POST2event_ical());// on remplace...
				}else{ //si c'est d'un calendrier à un autre
					if($metadonees_calendriers[$_POST["cal_base"]]["type"]=="local")if (droit_cal($_POST["cal_base"])<3)remplace_event($cal_base, $_POST["id_base"], "");//supression dans ancien calendrier
					insert_event(POST2event_ical(),$cal_cible);//creation dans le nouveau...
				}
			break;		
			case "suprimer":
				if($metadonees_calendriers[$_POST["cal_base"]]["type"]=="local")if (droit_cal($_POST["cal_base"])<3)remplace_event($cal_cible, $_POST["id_base"], "");//supression...				
			break;
		}
	}else{
		/** tentative de hack html malveillant ... à punir ! */
	}
}

$admin=false;
if(isset($_SESSION['log'])){
	if($_SESSION['niv']<2 && isset($_POST["admin"])){ //Interface d'administration...
		$corps=include("res/admin.php");
		$traiter_cal=false;
		$admin=true;
	}elseif(isset($_POST["compte_perso"])){//gestion des comptes perso (changement d mot d passe...)
		include("res/compte_perso.php");
		$traiter_cal=false;
	}
}


/////////////////////////////////////////////////////
//traitement calendrier, cache, html, affichage... //
/////////////////////////////////////////////////////
if($traiter_cal)$corps=include "res/traitement_cal.php";


//////////////////////////////////
// barre du haut de la page web //
//////////////////////////////////
include "res/res_menu.php";
$entete= '<div id="entete" ><div class="row">';
	$entete.='<div class="cell logo"><a href="https://git.framasoft.org/leiopar/MeshCal.git"><img id="logo_principal" style="width: 170px; height: 32px;" alt="MechCal" src="'.$theme.'logo_sobre_170x32.png"></a></div>';
	$entete.='<div class="b"> </div>';
	
	$entete.='<div class="cell g '.(!$admin&&($tab_req[0]=="J")?"on":"off").'">'.res_menu("<J").'</div>';/* jour */
	$entete.='<div class="cell m '.(!$admin&&($tab_req[0]=="J")?"on":"off").'">'.res_menu("J").'</div>';/* jour */	
	$entete.='<div class="cell d '.(!$admin&&($tab_req[0]=="J")?"on":"off").'">'.res_menu("J>").'</div>';/* jour */
	
	$entete.='<div class="b"> </div>';
	
	$entete.='<div class="cell g '.(!$admin&&($tab_req[0]=="S")?"on":"off").'">'.res_menu("<S").'</div>';/* semaine */
	$entete.='<div class="cell m '.(!$admin&&($tab_req[0]=="S")?"on":"off").'">'.res_menu("S").'</div>';/* semaine */
	$entete.='<div class="cell d '.(!$admin&&($tab_req[0]=="S")?"on":"off").'">'.res_menu("S>").'</div>';/* semaine */
		
	$entete.='<div class="b"> </div>';
	
	$entete.='<div class="cell g '.(!$admin&&($tab_req[0]=="M")?"on":"off").'">'.res_menu("<M").'</div>';/* mois */	
	$entete.='<div class="cell m '.(!$admin&&($tab_req[0]=="M")?"on":"off").'">'.res_menu("M").'</div>';/* mois */
	$entete.='<div class="cell d '.(!$admin&&($tab_req[0]=="M")?"on":"off").'">'.res_menu("M>").'</div>';/* mois */
	
	$entete.='<div class="b"> </div>';
	
	$entete.='<div class="cell g '.(!$admin&&($tab_req[0]=="A")?"on":"off").'">'.res_menu("<A").'</div>';/* année */
	$entete.='<div class="cell m '.(!$admin&&($tab_req[0]=="A")?"on":"off").'">'.res_menu("A").'</div>';/* année */
	$entete.='<div class="cell d '.(!$admin&&($tab_req[0]=="A")?"on":"off").'">'.res_menu("A>").'</div>';/* année */
	
	$entete.='<div class="b"> </div>';
	$entete.='<div class="cell dujour">';
	setlocale(LC_TIME, 'fr', 'fr_FR.UTF-8'); /* à internationaliser...*/
	$entete.='<b>'.lien_du_jour().'</b>'; // lien contextuel à l'affichage...
	$entete.='<span class="droite">';
	$entete.=include("res/res_popup.php");
	$entete.='</span>'; // toujour metre après tout traitement pour les message d'erreur !!!	
	$entete.='</div>';/* date du jour */
	
	/*icone de message et popup d'erreur */
	//$entete.='<div id="haut_droit">';
		$entete.='<div class="b"> </div>';
		/* config perso */
		
		
		
		if(isset($_SESSION['log']))$entete.='<div class="cell off"><form action="'.$query.'" method="post">'.
								'<input name="config" value="" type="hidden">'.
								'<label for="config" class="buster">config...</label>'.
								'<input id="config" type="submit" class="ghost">'.
							'</form></div>'.'<div class="b"> </div>';
		/* administration */
		if(isset($_SESSION['log']))if($_SESSION['niv']==0)$entete.='<div class="cell '.($admin?"on":"off").'"><form action="'.$query.'" method="post">'.
								'<input name="admin" value="" type="hidden">'.
								'<label for="admin" class="buster">admin...</label>'.
								'<input id="admin" type="submit" class="ghost">'.
							'</form></div>'.'<div class="b"> </div>';

		$entete.='<div class="cell" id="haut_droit">';

		$entete.=$session;
		$entete.='</div>';
	//$entete.= '</div>';
$entete.= '</div></div>';



/* generation finale de la page web */
echo $head;
echo $entete;
echo $corps;




?></body></html>