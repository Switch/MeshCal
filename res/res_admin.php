<?php

/* ajout d'un calendrier... */

function ajout_cal($type){
	global $bases, $rep_cal, $rep_cal_dist_sauv, $erreurs, $MeshCal_vertion;
	$err=false;
	$nom="";
	if ($type=="distant"){
		$adresse=trim($_POST["url"]); // l'url est testé en amont*** tester l'url ! */
		$nom_local=preg_replace('@[^a-zA-Z0-9_]@', '_', $adresse);
		$file=@file_get_contents($adresse);
		if($file===false){// si le fichier est indispo...
			$erreurs[]=array("type"=>"url_nouveau_cal_distant_ne_repond_pas", "url_cal"=>$adresse);	//message d'erreur
			$err=true;
		}else{
			$tab_lignes=explode("\n", trim($file)); //on separ le texte en lignes					
			if($tab_lignes[0]!="BEGIN:VCALENDAR"){ /*/ a rendre plus securitaire/sofistiqué ? ou pas ? */  //tester si source est bien un ICAL
				$erreurs[]=array("type"=>"url_nouveau_cal_distant_pas_ical", "url_cal"=>$adresse);
				$err=true;
			}else{

				$isMeshCalLike=false;
				file_put_contents($rep_cal_dist_sauv.$nom_local.".ics", $file); //enregistrement dans sauvegarde...
				foreach($tab_lignes as $ligne)if(trim($ligne)!="" && substr($ligne,0,1)!=" "){
					$pos = stripos($ligne, ":");				
					$variable=strtoupper(trim(substr($ligne,0,$pos)));
					$valeur=trim(substr($ligne,$pos+1));

					
					if($variable=="PRODID"){//est-ce un MeshCalLyke ? est-ce une version lisible ? teste de l'entète de fichier ICAL
						$tab_a=explode("//",$valeur); //si c'est un MeshCalLike, ça doit resembler à ça : PRODID:-//MeshCalLike/0.9//(autre truc optionel... site)
						$tab_b=isset($tab_a[1])?explode("/",$tab_a[1]):array();
						$isMeshCalLike=isset($tab_b[1])?($tab_b[0]=="MeshCalLike" && $tab_b[1]<=$MeshCal_vertion):false;
						/* evoyer un "coucou" à l'autre MeshCalLyke */
					}
					
					if (substr($variable,0,12)=="X-WR-CALNAME"){//(nom du calendrier)
						$nom=$valeur;
					}
				}
				
				$hash=hash('md4', $file);
				file_put_contents($rep_cal_dist_sauv.$nom_local.".ics".".hash", $hash."\n".time()."|".date("Y-m-d G:i:s")); // somme de somme de control du fichier distant...
				if($isMeshCalLike){
					$file_hash_distant=@file_get_contents($adresse.".hash");
					if($file_hash_distant===false){// si le fichier est indispo, c'est pas un MeshCalLike... ou alors il est corompu
						$isMeshCalLike=false; /* faire un message d'erreur ? */
					}else{
						$tab_lignes=explode("\n", trim($file_hash_distant));
						$isMeshCalLike=false;
						if($hash==$tab_lignes[0])if($hash==$tab_lignes[0])if(isset($tab_lignes[1])){//si les hash distant et calculé en local sont négaux
							$tab=explode("|",$tab_lignes[1]);
							if(is_int($tab[0]))$isMeshCalLike=true;//si le premier terme de la deuxième ligne est un antier, il y à de forte chance que ce soit un timestamp...
						}/* faire un message d'erreur ? */
					}
				}
				if($isMeshCalLike)$type="MeshCalLike";
				$nom=($nom==""?'Nom d\'Agenda...':$nom); /* trouver un autre non si vide... Conteznu de la balise html "<title>" dur site père... */			
			}
		}

	}else{//..."local" 
		$nom=str_replace('|','&#124;',trim($_POST["nom"]));//supression des "|" à cause des bases.csv.
		$adresse=preg_replace('@[^a-zA-Z0-9_]@', '_', $nom).".ics";
	
		$fich="BEGIN:VCALENDAR\n";
		$fich.="";
		$fich.="VERSION:2.0\n";
		$fich.="PRODID:-//MeshCalLike/".'0.9'."//(autre truc optionel... site)\n"; /***/
		$fich.="X-WR-CALNAME:".$nom."\n";
		$fich.="X-WR-TIMEZONE:Europe/Paris\n";
		$fich.="CALSCALE:GREGORIAN\n";
		//$fich.="X-WR-CALDESC:(description optionelle)\n";
		$fich.="END:VCALENDAR";
		
		file_put_contents($rep_cal.$adresse, $fich); // création du calendrier proprement dit !		
		$hash=hash('md4', $fich);
		file_put_contents($rep_cal.$adresse.".hash", $hash."\n".time()."|".date("Y-m-d G:i:s")); // somme de somme de control du fichier ...
	}
	
	if(!$err){
		$id=num_cal_libre();
		//echo $id."<br>";
		$coul=rand_coul();
		//echo "url : ".$_POST["url"]."<br>";
		
		
		// ajour dans couleurs_cal.css
		$fichier=$rep_cal."couleurs_cal.css";
		$contenu=trim(file_get_contents($fichier));
		$contenu.="\n.".$id.'{color:'.$coul["text"].';background-color: #'.$coul["fond"].';}';
		file_put_contents($fichier, $contenu);	
		
		// ajout dans bases/calendriers.csv
		$fichier=$bases."calendriers.csv";
		$contenu=trim(file_get_contents($fichier));
		$contenu.="\n".$id.' | '.$nom.' | '.$adresse.' | '.$type.' |';
		file_put_contents($fichier, $contenu);
		// cal_2 ; Agenda du libre Rhône-Alpes ; http://www.agendadulibre.org/ical.php?region=22 ; distant ;
	}
	
	
	
	
	
	

}









/////////////////////////////////////////////////////
// trouve le plus petit numero de calendrier libre //
/////////////////////////////////////////////////////
function num_cal_libre(){
	global $metadonees_calendriers;
	$i=1;
	while(isset($metadonees_calendriers["cal_".$i]))$i++;
	return ("cal_".$i);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// crée un couple de couleur text/fond. text est noir ou blanc selon le fond ; fond est au hazard //
////////////////////////////////////////////////////////////////////////////////////////////////////
function rand_coul(){ 
	$r_dec=rand ( 0 , 15 );		$r_hex=dechex ($r_dec);
	$v_dec=rand ( 0 , 15 );		$v_hex=dechex ($v_dec);
	$b_dec=rand ( 0 , 15 );		$b_hex=dechex ($v_dec);	
	//textlisible(r,v,b) ça sort d'un de mes vieux truc en JS : http://leiopar.free.fr/j904_outil_web_couleur.php
	if (((1.6*$r_dec*255/15)+(1.5*$v_dec*255/15)+(0.7*$b_dec*255/15))>382){
		$coul["text"]="black";
	}else{
		$coul["text"]="white";
	}
	$coul["fond"]=$r_hex.$v_hex.$v_hex;
	return $coul;
}




?>
