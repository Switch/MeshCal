<?php
//MeshCal
// construction des affichage calendrier en web










//////////////////////////////////////
// met le jour courant en valeur... //
//////////////////////////////////////
function style_jour_courant(){
	echo '<style type="text/css">#jour_'.(strtotime('today')+100).'{border: 2px solid red;background-color: rgb(255, 150, 150);}</style>'; /* A rendre configurable avec un fichier dans "themes/..." */
}



///////////////////////////////////////////////////////////////////////////////////////////////////////
// fonction d'affichage par jour ; $cal_aff= tableau de dates de référence pour la plage d'affichage //
///////////////////////////////////////////////////////////////////////////////////////////////////////
function tabs2html_jours($cal_aff){
	global $liste_evens_description, $liste_evens_occurences;
	
	/*echo '<hr>$liste_evens_description<pre>';
	print_r($liste_evens_description);
	echo "</pre>";
	
	echo '<hr>$liste_evens_occurences<pre>';
	print_r($liste_evens_occurences);
	echo "</pre>";*/
	
	$html="";
	setlocale(LC_TIME, 'fr', 'fr_FR.UTF-8'); /* à internationaliser...*/	

	$html.='<div class="cal">';
	$html.='<div class="t_row">'; //ligne de collones de jours...
	for($date = $cal_aff["debut_horizon"]+100; $date <= $cal_aff["fin_horizon"]+100; $date = strtotime("+1 day", $date)){// pour chaque jour à afficher
			
		$horizon=($date<$cal_aff["debut"]+100||$date>$cal_aff["fin"]+100);
		/*** integre les entètede "dimanche" */

		$html.='<div class="'.
			($horizon?'jour_horizon':'jour').
			'" id="jour_'.date("n", $date).'"'.
			'><h1 class="entete">'; //entète...
			
				/*echo ((date("w", $date)=="0")?'<div class="dimanche">':"");*/
			$html.=($horizon?'<a href="'.'?aff=J-'.strftime("%Y-%m-%d", $date).'&">':"").utf8_encode(strftime("%A %d", $date)).($horizon?'</a>':"");	// jour
				/*echo ((date("w", $date)=="0")?'</div>':"");	//fin de dimanche	*/		
			$html.=' <a href="'.'?aff=M-'.strftime("%Y-%m", $date).'&" class="mois">'.utf8_encode(strftime("%B", $date)).'</a>';	// mois
			$html.=' <a href="'.'?aff=A-'.strftime("%Y", $date).'&" class="annee">'.strftime("%Y", $date).'</a>';	// année
			$html.=' <a href="'.'?aff=S-'.strftime("%Y", $date)."-".date("W", $date).'&" class="semaine">(S'.date("W", $date).')</a>';	// semaine
			
			$html.='</h1>';	// fin d'entète...					

			/* conception à revoir avec des pro du html... */
			$h=6; /* a rendre configurable */

			if(isset($liste_evens_occurences["jour_".$date]))foreach($liste_evens_occurences["jour_".$date] as $horaire => $events){
				$heure=substr($horaire,6);
				$pref=($heure>-1)?gmdate("H:i ",$heure):""; /***** /!\ "date()" donne une heure de trop !!! utiliser gmdate() dans le reste du code ??? */  // heure de l'evenement...
				$heureH=($horaire>-1)?gmdate("H",$heure):"";
				
				foreach($events as $id => $e){ //mise en place des events...
					if ($pref!="")while($h<=$heureH){ //replisage des cases d'heures précédentes
						$html.=$h>6?'</div>':"";
						$html.='<div class="heure"><!--['.($date+$h*60*60).']-->';
						$html.=$h.":00"; /* conception à revoir avec des pro du html... */
						$h++;						
					}

					$description= /* faire mieux... (Nom de l'agenda, lieux, url...)*/
							(isset($liste_evens_description[$id]["VEVENT"]["DESCRIPTION"])?$liste_evens_description[$id]["VEVENT"]["DESCRIPTION"]:"").
							(isset($liste_evens_description[$id]["VALARM"]["DESCRIPTION"])?$liste_evens_description[$id]["VALARM"]["DESCRIPTION"]:"");
					/* $description = eregi_replace("([[:alnum:]]+)://([^[:space:]]*)([[:alnum:]#?/&=])","<A HREF=\"\\1://\\2\\3\" TARGET=\"_blank\">\\1://\\2\\3</A>",$description); // rend les lien cliquable... mais plante les popup :'( */
					
					$id_cal=$liste_evens_description[$id]["VEVENT"]["CAL_ID"];
					$html.='&nbsp;<a href="#" id="'.$id.'" class="tooltip yellow-tooltip">'. /*** metre une balise dont la class à le nom du calendrier !!! */
						'<div class="'.$id_cal.'" >'.
							netoyage_divers($liste_evens_description[$id]["VEVENT"]["SUMMARY"]).
						'</div>';
							$html.='<span><h1>'.$pref." ".netoyage_divers($liste_evens_description[$id]["VEVENT"]["SUMMARY"]).
							'<!--{'.$id_cal.'}'.$id.'{/'.$id_cal.'}-->'.
							'</h1><hr><p class="description">';
							$html.=netoyage_divers($description);
							$html.='</p></span>';
					$html.='</a><br>';							
				}
			}
			for ($h;	$h <=24; $h++){ //heure par heure pour finir la journée ...
				$html.=$h>6?'</div>':"";//fin d'heure			
				$html.=$h<24?'<div class="heure"><!--['.($date+$h*60*60).']-->'.($h.":00"):""; //on s'arrette à 23H00...
				/* conception à revoir avec des pro du html... */
			}
		$html.='</div>'; //fin de jour
	}
	$html.='</div>'; //fin de la ligne
	$html.='</div>'; //fin du calendrier
	return $html;
}




/////////////////////////////////////////////////////////////////////////////////////////////////////
// fontion d'afichage par mois ; $cal_aff= tableau de dates de référence pour la plage d'affichage //
/////////////////////////////////////////////////////////////////////////////////////////////////////
function tabs2html_mois($cal_aff){
	global $liste_evens_description, $liste_evens_occurences;
	
	/*echo '<hr>$liste_evens_description<pre>';
	print_r($liste_evens_description);
	echo "</pre>";
	
	echo '<hr>$liste_evens_occurences<pre>';
	print_r($liste_evens_occurences);
	echo "</pre>";*/
	
	$html="";
	setlocale(LC_TIME, 'fr', 'fr_FR.UTF-8'); /* à internationaliser...*/	

	$html.='<div class="cal">';

	for($date = $cal_aff["debut_horizon"]+100; $date <= $cal_aff["fin_horizon"]+100; $date = strtotime("+1 day", $date)){// pour chaque jour à afficher
			
		$horizon=($date<$cal_aff["debut"]+100||$date>$cal_aff["fin"]+100);
		/*** integre les entètede "dimanche" */

		if (date("w", $date)=="1")$html.='<div class="t_row">';	//lundi...

		$html.='<div class="'.
			($horizon?'jour_horizon':'jour').
			'" id="jour_'.$date.'">';
			
		if (date("w", $date)=="0")$html.='<div class="dimanche">';	//dimanche...
			
			$html.='<h1 class="entete"><!--['.($date).']-->'; //entète...
			
				/*echo ((date("w", $date)=="0")?'<div class="dimanche">':"");*/
			$html.=($horizon?'<a href="'.'?aff=J-'.strftime("%Y-%m-%d", $date).'&">':"").utf8_encode(strftime("%A %d", $date)).($horizon?'</a>':"");	// jour
				/*echo ((date("w", $date)=="0")?'</div>':"");	//fin de dimanche	*/		
			$html.=' <a href="'.'?aff=M-'.strftime("%Y-%m", $date).'&" class="mois">'.utf8_encode(strftime("%B", $date)).'</a>';	// mois
			$html.=' <a href="'.'?aff=A-'.strftime("%Y", $date).'&" class="annee">'.strftime("%Y", $date).'</a>';	// année
			$html.=' <a href="'.'?aff=S-'.strftime("%Y", $date)."-".date("W", $date).'&" class="semaine">(S'.date("W", $date).')</a>';	// semaine
			
			$html.='</h1>';	// fin d'entète...					

			/* conception à revoir avec des pro du html... */
			//$h=6; /* a rendre configurable */

			if(isset($liste_evens_occurences["jour_".$date]))foreach($liste_evens_occurences["jour_".$date] as $horaire => $events){
				$heure=substr($horaire,6);
				$pref=($heure>-1)?gmdate("(H:i) ",$heure):""; /***** /!\ "date()" donne une heure de trop !!! utiliser gmdate() dans le reste du code ??? */  // heure de l'evenement...
				$heureH=($horaire>-1)?gmdate("H",$heure):"";
				
				foreach($events as $id => $e){ //mise en place des events...

					$description= /* faire mieux... (Nom de l'agenda, lieux, url...)*/
							(isset($liste_evens_description[$id]["VEVENT"]["DESCRIPTION"])?$liste_evens_description[$id]["VEVENT"]["DESCRIPTION"]:"").
							(isset($liste_evens_description[$id]["VALARM"]["DESCRIPTION"])?$liste_evens_description[$id]["VALARM"]["DESCRIPTION"]:"");
					/* $description = eregi_replace("([[:alnum:]]+)://([^[:space:]]*)([[:alnum:]#?/&=])","<A HREF=\"\\1://\\2\\3\" TARGET=\"_blank\">\\1://\\2\\3</A>",$description); // rend les lien cliquable... mais plante les popup :'( */
					
					$id_cal=$liste_evens_description[$id]["VEVENT"]["CAL_ID"];
					$html.='
					<div class="event">
						<a href="#" id="'.$id.'" class="tooltip yellow-tooltip">'. /*** metre une balise dont la class à le nom du calendrier !!! */
							'<div class="'.$id_cal.'" >'.
								"&nbsp;".$pref.netoyage_divers($liste_evens_description[$id]["VEVENT"]["SUMMARY"]).
							'</div>';
							$html.='<span>
								<h1>'.$pref." ".netoyage_divers($liste_evens_description[$id]["VEVENT"]["SUMMARY"]).
									'<!--{'.$id_cal.'}'.$id.'{/'.$id_cal.'}-->
								</h1>
								<hr>
								<p class="description">';
									$html.=netoyage_divers($description);
								$html.='</p>
							</span>
						</a>
					</div>';							
				}
			}

		$html.='</div>'; //fin de jour
		if (date("w", $date)=="0")$html.='</div></div>'; //fin de dimanche
		//if (date("w", $date)=="0" && !$horizon)$html.='</div>'; //fin de ligne
	}

	$html.='</div>'; //fin du calendrier
	return $html;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////
// fontion d'afichage par année ; $cal_aff= tableau de dates de référence pour la plage d'affichage //
//////////////////////////////////////////////////////////////////////////////////////////////////////
function tabs2html_annee($cal_aff){
	global $liste_evens_description, $liste_evens_occurences;
	$html="";
	$annee=strftime("%Y", $cal_aff["debut"]);	
	
	//setlocale(LC_ALL, 'fr_FR.UTF-8'); /* à internationaliser...*/
	setlocale(LC_TIME, 'fr', 'fr_FR.UTF-8'); /* à internationaliser...*/	
	
	$debut=true;
	$html.='<div class="cal">';
	
	$html.='<div class="t_row">'; //ligne de colones de mois...
	for($date = $cal_aff["debut_horizon"]+100; $date <= $cal_aff["fin_horizon"]+100; $date = strtotime("+1 day", $date)){// pour chaque jour à afficher
		if(date("j", $date)==1 || $debut){
			$html.='<div class="'.
						(($date<$cal_aff["debut"]||$date>$cal_aff["fin"])?'mois_horizon':'mois').
						'" id="mois_'.date("n", $date).'"'.
						'><h1 class="entete">'.
						'<a href="'.'?aff=M-'.strftime("%Y-%m", $date).'&">'.
							utf8_encode(strftime("%B", $date)).
						'</a>'.
						(($date<$cal_aff["debut"])?
							' <a href="'.'?aff=A-'.($annee-1).'&" class="annee">'.
								($annee-1).
							'</a>':'').
						(($date>$cal_aff["fin"])?
							' <a href="'.'?aff=A-'.($annee+1).'&" class="annee">'.
								($annee+1).
							'</a>':'').
						'</h1>';
			$debut=false;
		}
		$html.=(isset($liste_evens_occurences["jour_".$date])?"":'<div class="vide">').
			((date("w", $date)=="0")?'<div class="dimanche">':"").
				'<div id="jour_'.$date.'" class="jour">'.
					'<!--['.$date.']-->'.
					'<a href="'.'?aff=J-'.strftime("%Y-%m-%d", $date).'&" class="tooltip yellow-tooltip">'.
						utf8_encode(strftime("%A %d", $date)); /*** n'est pas à sa place !!! (va entréner des balise "a" imbriqué...)*/ //affichage de la semaine
						if(isset($liste_evens_occurences["jour_".$date])){ //s'il y à des event... infobule !
							$html.='<span><b>'.utf8_encode(strftime("%A %d %B %Y", $date)).'</b><hr>';
							foreach($liste_evens_occurences["jour_".$date] as $horaire => $events){//toute les heures à evenement(s)
								$heure=substr($horaire,6);				
								$pref=($horaire>-1)?gmdate("H:i ",$heure):""; /***** /!\ "date()" donne une heure de trop !!! utiliser gmdate() dans le reste du code ??? */ // heure de l'evenement...					
								foreach($events as $id => $e){ // tout les evenement de l'heure
									$html.='<div class="'.netoyage_divers($liste_evens_description[$id]["VEVENT"]["CAL_ID"]).'" >'.
											$pref. // heure de debut de l'evenement
											netoyage_divers($liste_evens_description[$id]["VEVENT"]["SUMMARY"]).
											'<!--{'.$liste_evens_description[$id]["VEVENT"]["CAL_ID"].'}'.$id.'{/'.$liste_evens_description[$id]["VEVENT"]["CAL_ID"].'}-->'.
											'</div>';
								}
							}
							$html.='</span>';
						}
					$html.='</a>'.
					((date("w", $date)=="1")?' <span class="semaine">S'.date("W", $date).'</span>':""). /*** atention au semain 01 qui comence l'année d'avant !!!*/ //affichage de la semaine
			((date("w", $date)=="0")?'</div>':"").	//fin de dimanche
		(isset($liste_evens_occurences["jour_".$date])?"":'</div>') //fin de "vide"
		.'</div>'. //fin de jour
		((date("j", strtotime("+1 day", $date))==1 || $date==($cal_aff["fin_horizon"]+100))?'</div>':""); //fin de mois	
	}
	$html.='</div>'; //fin de la ligne
	$html.='</div>'; //fin du calendrier
	
	return $html;
}


?>