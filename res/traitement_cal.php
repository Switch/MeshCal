<?php
//MeshCal
//gestion des calendriers...
// création des affichage, gestion des cache des erreurs...

include "res/res_ics.php"; //resources specialement pour traiter les fichier ICS
include "res/res_html.php"; //resources specialement pour creer le code HTML


//echo $url_tab['query']."<hr>";
//echo "<hr>contextualisation de dates<br>";



switch ($tab_req[0]) {
    case "A": //Année
		$cal_aff["debut"]=mktime(0, 0, 0, 1, 1, $tab_req[1]);//calcul du premier jour de l'année
		$cal_aff["debut_horizon"]=mktime(0, 0, 0, 12, 1, $tab_req[1]-1);//calcul du premier jour de la semaine du premie jour du mois
		$cal_aff["fin"]=mktime(0, 0, 0, 12, 31, $tab_req[1]);//calcul du dernier jour du mois
		$cal_aff["fin_horizon"]=mktime(0, 0, 0, 1, 31, $tab_req[1]+1);//calcul du dernier jour de la semaine du dernier jour du mois		
		style_jour_courant();
        break;
	case "M": //mois
		$cal_aff["debut"]=mktime(0, 0, 0, $tab_req[2], 1, $tab_req[1]);//calcul du premier jour du moi
		$j_s=(date("w", $cal_aff["debut"])==0?7:date("w", $cal_aff["debut"])); /* à internationaliser... */ /*voir "strftime" avec "%u" (et non "%w") */ // (numero de jour dans la semaine lundi =1, dimanche =7
		$cal_aff["debut_horizon"]= $cal_aff["debut"]-($j_s-1)*24*60*60;//calcul du premier jour de la semaine du premie jour du mois
		$cal_aff["fin"]=strtotime("-1 day", mktime(0, 0, 0, $tab_req[2]+1, 1, $tab_req[1]));//calcul du dernier jour du mois
		$j_s=(date("w", $cal_aff["fin"])==0?7:date("w", $cal_aff["fin"])); /* à internationaliser... */ /*voir "strftime" avec "%u" (et non "%w") */ // (numero de jour dans la semaine lundi =1, dimanche =7
		$cal_aff["fin_horizon"]=$cal_aff["fin"]+(7-$j_s)*24*60*60;//calcul du dernier jour de la semaine du dernier jour du mois
		style_jour_courant();
		break;
	case "S":
		$premier_jour_de_l_annee=mktime(0, 0, 0, 1, 1, $tab_req[1]);
		$j_s=(date("w", $premier_jour_de_l_annee)==0?7:date("w", $premier_jour_de_l_annee));
		if ($j_s>4){//semaine 01 commence l'année dernière
			$lundi_s0=strtotime("+".(8-$j_s)." day", $premier_jour_de_l_annee);
		}else{//semaine 01 commence cette année
			$lundi_s0=strtotime("-".($j_s-1)." day", $premier_jour_de_l_annee);
		}
		$cal_aff["debut"]=strtotime("+".(7*($tab_req[2]-1))." day", $lundi_s0);
		$cal_aff["debut_horizon"]=strtotime("-1 day", $cal_aff["debut"]);//La veille
		$cal_aff["fin"]   = strtotime("+6 day", $cal_aff["debut"]);// de dimanche
		$cal_aff["fin_horizon"]=strtotime("+7 day", $cal_aff["debut"]);//le lundi suivant
        break;		
	case "J":
		$cal_aff["debut"]=mktime(0, 0, 0, $tab_req[2], $tab_req[3], $tab_req[1]);//LE jour de l'année
		$cal_aff["debut_horizon"]=strtotime("-1 day", $cal_aff["debut"]);//La veille
		$cal_aff["fin"]=$cal_aff["debut"];//un seul jour, forcément...
		$cal_aff["fin_horizon"]=strtotime("+1 day", $cal_aff["debut"]);//le lendemain
        break;
	
	case "L": /* a rendre plus souple... */
		echo "liste sur <i>N</i> jours"; /***/
		break;		
		
}



/*
echo "<pre>";
print_r($cal_aff);
echo "</pre><br>";
echo "debut         : ".utf8_encode(strftime("%A %d %B %Y", $cal_aff["debut"]))."<br>";
echo "debut horison : ".utf8_encode(strftime("%A %d %B %Y", $cal_aff["debut_horizon"]))."<br>";
echo "fin           : ".utf8_encode(strftime("%A %d %B %Y", $cal_aff["fin"]))."<br>";
echo "fin horizon   : ".utf8_encode(strftime("%A %d %B %Y", $cal_aff["fin_horizon"]))."<hr>";
*/



/** chargement des fichier calendrier au besoin... */
//début de traitement ics
$cache_ok=(count(glob($rep_cach."*.html"))>0);//si il y à eu un changement en local, il n'y à plus de cache
/***if ($cal["type"]=="local") si il y à eu un changement en local, il n'y à plus de cache */
/*** traiter les fichier cache inabituel (mois +1, +2, +3... -1, -2, -3... eccaetera */







$cache_ok=true; //reste vrai si on peu utiliser le cache, sinon... recalcul !

// première passe determination si les calendrier distant ont changer et chargement des calendriers distants
foreach($metadonees_calendriers as $id_cal => $cal){
	$er=array();
	if ($cal["type"]!="local"){ // si calendrier d'un autre site...
		
		$fichier=$rep_cal_dist_sauv.$cal["nom_local"].".ics".".hash"; //fichier hash local
		if (file_exists($fichier)){//le fichier hash local existe il ?
			$hash["local"] = file_get_contents($fichier); //lecture de somme de control local
		}else{
			$cache_ok=false;
			$hash["local"]="local_vide";
		}
		
		if ($cal["type"]=="MeshCalLike"){ //calendrier distant de type MeshCal (MeshCal ou forks...)
			/*** controler si les fichiers "hash" existe !!! */
			
			$file=@file_get_contents($cal["adresse"].".hash"); //lecture de somme de control distant
						/** peu etre faire un netoyage de sécurité ? */
						/*** gerrer les erreur */
						/* le fichier récupéré est il bien un hash MeshCalLike ? (sinon => erreur !) */
			if($file===false){// si le fichier est indispo			
				echo "fichier introuvable !!!"; /***** gerrer les erreur */
				/** resilience */
				/* creer un message d'erreur */

				$hash["distant"] = $hash["local"];
			}else{
				$hash["distant"] = $file;
				/** les fichiers hash MeshCal doivent contenir une date */
			}
		}
		
		if ($cal["type"]=="distant"){ //calendrier distant divers
			if($developement){
				$file=false;
				
			}else{
				$file=@file_get_contents($cal["adresse"]);
						/** peu etre faire un netoyage de sécurité ? */
						/* le fichier récupéré est il bien un hash MeshCalLike ? (sinon => erreur !) */				
			}
			

			if($file===false){// si le fichier est indispo
				$erreurs[]=array("type"=>"fichier_distant_introuvable","id_cal"=>$id_cal, "url_cal"=>$cal["adresse"]);
				//echo "fichier introuvable !!!"; /***** gerrer les erreur */
				/** resilience */
				// creer un message d'erreur */
				$er=array("type"=>"cal_distant_absent", "id_cal"=>$id_cal);
				/* chargé la dernière sauvegarde */
				$hash["distant"] = $hash["local"];
				$file=file_get_contents(
					$rep_cal_dist_sauv.
						$metadonees_calendriers[$id_cal]["nom_local"].".ics");
			}//else{
				$metadonees_calendriers[$id_cal]["file"]=$file; /*** peu etre faire un netoyage de sécurité ? */
				$hash["distant"] = hash('md4', $cal["file"]); // calcul de somme de control du fichier distant...
				/** les fichiers hash MeshCal doivent contenir une date */
			//}
		}
		
		if ($hash["distant"] != $hash["local"]){ //comparaison des hashage md4
			if ($cal["type"]=="MeshCalLike")$metadonees_calendriers[$id_cal]["file"]=file_get_contents($cal["adresse"]); //(télé)chargement du dernier calendrier MeshCalLike...
			$cache_ok=false;
		}// sinon $cache_ok inchanger... si deja "false" ==> reste "false" !
	}
	if (count($er) > 0)$erreur[]=$er; //ajour à la liste d'erreur général
}






///////////////////////////////////////////////////////////////////////
// deuxieme passe si nécéssaire pour finir de charger les calendrier //
///////////////////////////////////////////////////////////////////////
if(!$cache_ok)foreach($metadonees_calendriers as $id_cal => $cal){
	
	if ($cal["type"]=="local"){
		$metadonees_calendriers[$id_cal]["file"]=file_get_contents($rep_cal.$cal["adresse"]); //(télé)chargement des calendrier locaux.
	}else{
		if($cal["file"]==""){ //s'il y à eu du changement,
			$metadonees_calendriers[$id_cal]["file"]=file_get_contents($cal["adresse"]); /***** gerrer les erreur */ //(télé)chargement des dernier calendrier.
		}else{
			if ($cal["type"]!="local")file_put_contents($rep_cal_dist_sauv.$cal["nom_local"].".ics".".hash", hash('md4', $metadonees_calendriers[$id_cal]["file"]));// refaire les sommation des non locaux	
		}
		/*** sauvegarde de la vestion précédente */
		file_put_contents($rep_cal_dist_sauv.$cal["nom_local"].".ics", $cal["file"]); //enregistrement dans sauvegarde...
	}
}







///////////////////////
// traitement ics... //
///////////////////////
if(!$cache_ok){//il faut régénéré les caches... donc on retraite les ics.
	foreach($metadonees_calendriers as $id_cal => $cal)// pour tous les calendriers... convertion en tableau.
		IcsTab2Tabs($cal["file"],$id_cal,$cal_aff["debut_horizon"],$cal_aff["fin_horizon"]+(60*60*24)); /*** pourquoi "60*60*24" ??? sinon ca oubli le dernier jour !!! */ //parse les chaines ics en alimentant les deux tableaux principaux...
	
	ksort($liste_evens_occurences); //trie par jours	
	foreach($liste_evens_occurences as $j => $h)ksort($liste_evens_occurences[$j]); //trie par heures
	

	switch ($tab_req[0]) {
		case "A": //Année
			$ret='<link rel="stylesheet" type="text/css" href="'.$theme.'annee.css" media="all">'; /*a rendre configurable*/ //styles...
			$ret.=tabs2html_annee($cal_aff); //affichage html sur un an.
			break;
		case "M": //mois
			$ret='<link rel="stylesheet" type="text/css" href="'.$theme.'mois.css" media="all">'; /*a rendre configurable*/ //styles... 
			$ret.=tabs2html_mois($cal_aff); /* affichage en jours */
			break;
		case "S": //semaine
			$ret='<link rel="stylesheet" type="text/css" href="'.$theme.'jours.css" media="all">'; /*a rendre configurable*/ //styles...
			$ret.=tabs2html_jours($cal_aff); /* affichage en jours */		
			break;
		case "J": //Journée
			$ret='<link rel="stylesheet" type="text/css" href="'.$theme.'jours.css" media="all">'; /*a rendre configurable*/ //styles...
			$ret.=tabs2html_jours($cal_aff); /* affichage en jours */
			break;
		case "L":
			$ret="liste sur <i>N</i> jours";
			
			break;
	}

	/***********************************/
	/*** création des fichier cache... */
	/***********************************/
}





///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// si connecté, ajouter les boutons "[+]" pour permetre la création(et modification) d'event avec monkeydate //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
if($nb_cal["locaux"]>0)if(isset($_SESSION['log']))if($_SESSION['niv']<6){ //*** si seulement il existe au moin un calendrier local, si conecté et non banni...
	

	$droit_edition_event=$_SESSION['niv'];
	foreach($privilege as $i)$droit_edition_event=min($i, $droit_edition_event);

	//0, 1	admin
	//2		création d'event dans un cal
	//3		modification d'event dans un cal
	//4		ajout d'information dans un cal
	//(5)	pas de privilège particulier (état par defaut)
	//(6, 7)banni, tout privilège bloqué par defaut
	if($droit_edition_event<=2){ // si droit d'ecriture dans au moins un calendrier...
		$form["debut"]='
			<form method="post" action="'.$query.'" class="appelle_monkeydate_creer">
				<input type="hidden" name="Monkey_Date" value="creation">
				<input type="hidden" name="origine" value="'.
					$query. /* peu etre inutile ? */
				'" />
				<input type="hidden" name="date" value="';
		$form["fin"]='" />
				<input type="submit" value="+" />
			</form>';

		$ret=str_replace("<!--[",$form["debut"],$ret);
		$ret=str_replace("]-->",$form["fin"],$ret);
	}
	


	
	// pour tout les calendrier autorisé en modification
		/* faire aparaitre les boutons de modification */

	foreach($metadonees_calendriers as $id_cal => $cal){
		$modif=false;
		if (isset($privilege[$id_cal]))if($privilege[$id_cal]<=4) $modif=true; // si minimum droit d'ajout d'info dans le calendrier...		
		if ($droit_edition_event<=2 || $modif){ // si minimum droit de création d'event dans au moins un calendrier...
			$form["debut"]='
				<form method="post" action="'.$query.'" class="appelle_monkeydate_modifier">
					<input type="hidden" name="Monkey_Date" value="modification" />
					<input type="hidden" name="origine" value="'.
						$query. /* peu etre inutile ? */
					'" />'.
					'<input type="hidden" name="cal_event" value="'.$id_cal.'" />'.
					'<input type="hidden" name="id_event" value="';
			$form["fin"]='" />
					<input type="submit" value="m" />
				</form>';
			
			// <!--{id_cal}id_event{/id_cal}-->
			$ret=str_replace("<!--{".$id_cal."}".$id_cal."|",$form["debut"],$ret);
			$ret=str_replace("{/".$id_cal."}-->",$form["fin"],$ret);
		}
	}
}


/* legende */
if (count($metadonees_calendriers)>1){
	$ret.='<hr><b>Calendriers : </b>'."\n";
	foreach($metadonees_calendriers as $id => $meta) $ret.='<span class="'.$id.'" >'.$meta['nom'].'</span><span class="exp">(<a href="'.$meta["adresse"].'">ical</a>)</span> ; '."\n"; /* metre des bouton de création d'event ? */
}

//////////////////////////////
// Génération de la page... //
//////////////////////////////
return $ret;




//echo '<hr>$metadonees_calendriers<pre>';
//print_r($metadonees_calendriers); //liste des calendrier au format ICAL [nom], [adresse], [type], [contenu_fic], [style]...
//echo '</pre>';

//echo '<hr>$liste_evens_description<pre>';
//print_r($liste_evens_description); //Detail des evenements ; une entrès par evenement ; rangé par "nom du calendrier"."|(tube)"."UID"
//echo '</pre><hr>$liste_evens_occurences<pre>';
//print_r($liste_evens_occurences);
//echo '</pre><hr>';


?>