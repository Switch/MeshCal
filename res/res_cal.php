<?php
//MeshCal
// manipulation des fichier ical laucaux



//creer les premier tableaux :
//		$tab["cal"] ressance les calendrier et leur "m�tadonn�es" g�n�rales
//		$tab["privileges"] decrit les privil�ge de l'utilisateur courant calendrier par calendrier
//
//				0, 1	admin
//				2		cr�ation d'event dans un cal
//				3		modification d'event dans un cal
//				4		seulement ajout d'information dans un cal
//				(5)		(depart) pas de privil�ge particulier (�tat par defaut)
//				(6, 7)	(depart) banni, tout privil�ge bloqu� m�me ceux defini par les config de calendrier
//
//		$tab["nb"]["totaux"] donne le nombre de calendrier locaux
//		$tab["nb"]["locaux"] donne le nombre de calendrier locaux
//		$tab["nb"]["distant"] donne le nombre de calendrier locaux

function listage_metadonees_calendriers(){
	global $bases;
	$tab["cal"]=array();
	$tab["privileges"]=array();
	$tab["nb"]["totaux"]=0;
	$tab["nb"]["locaux"]=0;
	$tab["nb"]["distant"]=0;
	if ($cal = fopen($bases.'calendriers.csv', 'rb')){ //liste de tout les calendrier � afich� avec leur m�tadonn�es
		while (($i = fgetcsv($cal, 1000, "|")) !== FALSE){ // parcour...
			$id_cal=trim($i[0]);
			if (substr($id_cal,0,2)!="//" && substr($id_cal,0,1)!="#" && $id_cal!=""){// si ce n'est pas une ligne de comentaire ou une ligne vide alors on renseigne les metadonn�es...
				$tab["cal"][$id_cal]["nom"]=trim($i[1]);
				$tab["cal"][$id_cal]["adresse"]=trim($i[2]);
				$tab["cal"][$id_cal]["type"]=trim($i[3]);
				$tab["nb"]["totaux"]++;
				if($tab["cal"][$id_cal]["type"]!="local"){ //si canendrier distant
					$tab["nb"]["distant"]++;
					$tab["cal"][$id_cal]["nom_local"]=preg_replace('@[^a-zA-Z0-9_]@', '_', $tab["cal"][$id_cal]["adresse"]); /*** nom de fichier localis� netoyer � traiter et sauvegarder ailleur... */
				}else{ //sinon si calendrier local on renseigne les privil�ge d'�dition acord� � l'utilisateur courant
					$tab["nb"]["locaux"]++;
					if(isset($_SESSION['log']))if($_SESSION['niv']<=5){//si logu� et pas banni on liste les autorisations
						$liste=substr($tab["cal"][$id_cal]["adresse"], 0, -4).".csv"; //(nom du fichier contenant la liste des utilisateurs du calendrier)
						if (file_exists($bases.'cal/'.$liste)){
							$users = fopen($bases.'cal/'.$liste, 'rb');
						
						//if (!($users===false))
							
							while (($j = fgetcsv($users, 1000, "|")) !== FALSE){// parcour de la liste
								$id_user=trim($j[0]);
								if (substr($id_user,0,2)!="//" && substr($id_user,0,1)!="#" && $id_user!=""){ // si ce n'est pas un comentaire...
									if ($id_user=="@all" || $id_user==$_SESSION['log']){ /*atention � interdir la cr�ation d'un utilisateur dont l'identifiant commence par "@" */ //le meta utilisateur "@all" designe des droits donn�s � tous par defaut
										$niv=$tab["privileges"][$id_cal]=((isset($j[1])?trim($j[1]):"")==""?2:trim($j[1]));
										$tab["privileges"][$id_cal]=(isset($tab["privileges"][$id_cal])?min($tab["privileges"][$id_cal],$niv):$niv); //au choix on prend le plus permissif
									}
								}
							}
							fclose($users);
						}
						
					}
				}
				$tab["cal"][$id_cal]["file"]=""; //cr�ation de la place pour le contenu des fichier
			}
		}
		fclose($cal);
	}
	
	/*echo "<hr>metadonees_calendriers<pre>";
	print_r($tab);
	echo "</pre><hr>";*/
	
	return $tab;
}




////////////////////////////////////////////////////////////////
// netoyage de contenu textuel venant de calendrier divers... //
////////////////////////////////////////////////////////////////
function netoyage_divers($text,$type="html"){
	$text=str_replace('\,',',',$text);
	$text=str_replace('\;',';',$text);
	$text=str_replace("&amp;","&",$text);
	if ($type=="html"){
		$text=str_replace('&#13;','<br>',$text);
	}else{
		$text=str_replace('&#13;'."\n","\n",$text);
		$text=str_replace('&#13;',"\n",$text);
	}
	return $text;
}




//////////////////////////////////////////////////////////////////
// determine les droits de l'utilisateur sur le calendrier $cal //
//////////////////////////////////////////////////////////////////
function droit_cal($cal){
	global $privilege, $bases, $metadonees_calendriers;

	$lvl=7; //niveau par defaut (7 = bannissement ultime)	
	if($metadonees_calendriers[$cal]["type"]=="local"){	// control� si calendrier bien local
		if (isset($_SESSION['niv'])){	 //si l'utilisateur est connect�
			if ($csv = fopen($bases.'utilisateurs.csv', 'rb')){	// control si pas banni entretemps !!!
				while (($i = fgetcsv($csv, 1000, "|")) !== FALSE)
					if ($_SESSION['log'] == $i[0]) $_SESSION['niv']=$i[3];
				fclose($csv);
			}else{
				/*sinon c'est que la base est inaxessible */
			}
			$lvl=$_SESSION['niv'];
			if ($_SESSION['niv']<6)	//si pas banni
				if(isset($privilege[$cal]))$lvl=min($lvl, $privilege[$cal]);
		}else{
			/* tentative de hack html malveillant ... � punir ! */
		}
	}else{
		/* tentative de hack html malveillant ... � punir ! */
	}
	return $lvl;	
}








///////////////////////////////////////////////////////////////////////////////////////////////
// convertion d'un evenement venu de Monkey_Date dans $_POST en un bloc texte de type "ical" //
///////////////////////////////////////////////////////////////////////////////////////////////
function POST2event_ical(){
	/* pour traiter les erreures... */

	$even="BEGIN:VEVENT\n";
	$even.="CREATED:".date("Ymd\THis")."Z\n";
	$even.="UID:".date("YmdHis-").rand( 1000000 , 9999999 )."@".$_SERVER['HTTP_HOST']."\n"; 
	$even.="X-auteur:".$_SESSION['log']."\n";
	//$even.="X-validateur:"."jerome(ppra)"."\n"; /***/
	$titre=str_replace(":",'&#58;',$_POST['titre']); // remplacement des caractaires ":"
	$even.="SUMMARY:".htmlspecialchars($titre!=""?$titre:"(Nouvel �v�nement)")."\n";  /* couper en ligne de 74 caractaires + un " " au debut */ /** erreur si vide */
	/*echo "<hr><pre>";print_r($_POST); echo "</pre>";*/
	if($_POST['repet']!="jamais"){
		
		if($_POST['repet']=="jour")$even.="RRULE:FREQ=DAILY;INTERVAL=".$_POST['chaque']; //repetition tout les n jours
		if($_POST['repet']=="semaine")$even.="RRULE:FREQ=WEEKLY;INTERVAL=".$_POST['chaque'].";BYDAY=".implode(',', $_POST['jours']); /*** traitement si $_POST['jours'] vide !!! */ //repetition tout les n semaine
		if($_POST['repet']=="mois"){
			if($_POST['choix_repet']=="xeme")$even.="RRULE:FREQ=MONTHLY;INTERVAL=".$_POST['chaque'].";BYDAY=".$_POST['jour_a'].$_POST['jour_b']; //repetition tout le X�me jour tout les n mois
			if($_POST['choix_repet']=="num")$even.="RRULE:FREQ=MONTHLY;INTERVAL=".$_POST['chaque'].";BYMONTHDAY=".implode(',', $_POST['j']); //repetition tout les jour numero X tout les mois
		}
		if($_POST['repet']=="an"){
			//repetition tout le X�me jour tout les x mois tout les n ans comme : RRULE:FREQ=YEARLY;INTERVAL=2;BYMONTHDAY=15;BYMONTH=11
			if($_POST['choix_repet']=="xeme")$even.="RRULE:FREQ=YEARLY;INTERVAL=".$_POST['chaque'].";BYDAY=".$_POST['jour_a'].$_POST['jour_b'].";BYMONTH=".implode(',', $_POST['m']);
			//repetition tout les jour numero X tout les x mois tout les n ans
			if($_POST['choix_repet']=="num")$even.="RRULE:FREQ=YEARLY;INTERVAL=".$_POST['chaque'].";BYMONTHDAY=".implode(',', $_POST['j']).";BYMONTH=".implode(',', $_POST['m']);
		}
		if($_POST['repet_fin']=="nb")$even.=";COUNT=".$_POST['fin_nb_unit'];
		if($_POST['repet_fin']=="date"){
			$datef=DateTime::createFromFormat("d/m/Y", $_POST['date_fin']);
			$datef->add(new DateInterval('P1D'));
			$datef=$datef->format("Ymd");
			$even.=";UNTIL=".$datef."T000000Z";
		}
		$even.="\n";
	}
	
	$date1=DateTime::createFromFormat("d/m/Y", $_POST['date1']);
	$date1=$date1->format("Ymd");
	$date2=DateTime::createFromFormat("d/m/Y", $_POST['date2']);
	$date2=$date2->format("Ymd");
	if (!isSet($_POST['journee'])){
		$date1.="T".$_POST['heure1'].$_POST['minute1']."00";
		$date2.="T".$_POST['heure2'].$_POST['minute2']."00";
	}
	//$even.="DTSTART;TZID=Europe/Paris:".min($date1,$date2)."\n"; /*** timezone */
	//$even.="DTEND;TZID=Europe/Paris:".max($date1,$date2)."\n"; /*** timezone */
	$even.="DTSTART:".min($date1,$date2)."\n";
	$even.="DTEND:".max($date1,$date2)."\n";
	
	
	if($_POST['lieu']!="")$even.="LOCATION:".$_POST['lieu']."\n"; /* openstreetmap ? */
	if($_POST['url']!="")$even.="URL:".$_POST['url']."\n";	
	
	$description=htmlspecialchars($_POST['description']);
	$description=str_replace( array(chr(13).chr(10) , chr(13) , chr(10)) , '&#13;' , $description);
	$description=str_replace(":",'&#58;',$description);
	
	if($_POST['description']!="")$even.="DESCRIPTION:".$description."\n"; /*** couper en ligne de 74 caractaires + un " " au debut */
	if (isSet($_POST['journee']))$even.="TRANSP:TRANSPARENT"."\n";
	$even.="BEGIN:VALARM\nACTION:DISPLAY\nTRIGGER;VALUE=DURATION:-P3D"."\n"; /** a rendre plus int�ligent : une alarme le lendemain de la cr�ation puis 3 jour avant (5 si week-end) */
	$even.="END:VALARM\nEND:VEVENT\n";
	

	//echo "<hr><pre>";	/***/
	//print_r($even);		/***/
	//echo "</pre><hr>";	/***/
	
	return $even;
}






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// insertion d'un nouvel evenement format� en un bloc texte de type "ical"($even) dans un fichier calendrier ($fic_cal) //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function insert_event($even,$fic_cal){
	$moa = "";
	if ($fic=fopen($fic_cal, "r")) {
		while (($buffer = fgets($fic, 4096)) !== false) {
			if ( substr($buffer, 0 , 13) =="END:VCALENDAR")$moa.=$even;
			$moa.= $buffer;
		}
		if (!feof($fic)) {
			echo "Erreur: fgets() a �chou�\n";
		}
		fclose($fic);
		file_put_contents($fic_cal, $moa);
	}else{
		echo "erreur de fichier dans insert_event !<br>"; /*** creer un message d'erreur... */
	}
}









///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Remplace (ou efface si "$nouveau" est vide au lieu de contenir l'evenement remplacent) l'evenement dont l'id est "$uid" dans le fichier calendrier "$cal" //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function remplace_event($cal, $uid, $nouveau=""){
//echo "<hr>".$cal; /***/
	$handle = fopen($cal, "r"); //ouverture du fichier calendrier
	if ($handle) {
		$levent="";$buf="";$bevent=false;
		while (($lcal = fgets($handle, 4096)) !== false) { //parcour ligne par ligne...

			if (substr($lcal, 0, 12) =="BEGIN:VEVENT")$bevent=true; //si debut d'un event
			if (substr($lcal, 0, 4) =="UID:")$id=trim(substr($lcal, 4, -1)); //r�cup�ration de l'id de l'event
			
			if ($bevent){ //si on est dans un event, on enregistre en concatenant dans la variable event
				$levent.=$lcal;
			}else{ // sinon c'est que se sont des ligne concernant l'ensamble du calendrier, on met dans le bufer
				$buf.=$lcal;
			}
			
			if ( substr($lcal, 0 , 10) =="END:VEVENT"){ //si c'est la fin de l'event 
				if ($id!=$uid){//et que ce n'est pas l'event d�sir� on buferise l'event,
					$buf.=$levent;
				}else{ //sinon c'est que c'est celui dont on ne veux plus donc on le remplace
					$buf.=$nouveau;
				}
				$levent="";$bevent=false; //et on se pr�par pour les event suivant
			}
			
		}
		if (!feof($handle)) { /* gestion d'erreur � g�rer... */
			echo "Erreur: fgets() a �chou�\n";
		}
		fclose($handle); //fermeture du calendrier
		//echo "<hr><pre>".$buf."</pre>"; /***/
		file_put_contents($cal, $buf);/*** � s�curiser !!! ***/ //ecrasement du calendrier avec le bufer... qui est sans l'event � suprimer
	}                                                            
}               



///////////////////////////////////////////////////////////////////////////////////////////////
// Extretrait le contenu d'un event pr�ci d'un calendrier donn� au format bloc de texte ical //
///////////////////////////////////////////////////////////////////////////////////////////////
function capture_event($calid, $uid){
	global $metadonees_calendriers, $rep_cal_dist_sauv;
//echo "<hr>".$cal; /***/
	$handle = @fopen($metadonees_calendriers[$calid]["adresse"], "r"); /*ouverture de la sauvegarde local si distant indisponible ! *///ouverture du fichier calendrier distant
	if (!$handle) 	$handle = fopen($rep_cal_dist_sauv.$metadonees_calendriers[$calid]["nom_local"].".ics", "r");

	$levent="";$buf="";$bevent=false;
	while (($lcal = fgets($handle, 4096)) !== false) { //parcour ligne par ligne...

		if (substr($lcal, 0, 12) =="BEGIN:VEVENT")$bevent=true; //si debut d'un event
		if (substr($lcal, 0, 4) =="UID:")$id=trim(substr($lcal, 4, -1)); //r�cup�ration de l'id de l'event
		
		if ($bevent && 									//si on est dans un event...
			(substr($lcal, 0, 12) !="BEGIN:VEVENT") && 	//mais ni sur la premi�re ligne...
			(substr($lcal, 0 , 10) !="END:VEVENT")		//ni sur la derni�re...
		)$levent.=$lcal; 								//alors on enregistre en concatenant dans la variable event
		
		if (substr($lcal, 0 , 10) =="END:VEVENT"){ //si c'est la fin de l'event 
			if ($id==$uid)$event=$levent; /* faire un breack ? */ //et que c'est l'event d�sir� on enregistre...
			$levent="";$bevent=false; //et/ou on se pr�par pour les event suivant
		}
		
	}
	if (!feof($handle)) { /* gestion d'erreur � g�rer... */
		echo "Erreur: fgets() a �chou�\n";
	}
	fclose($handle); //fermeture du calendrier
	
	
	/*echo '<hr>l event : <pre>';
	print_r($event);
	echo "</pre>";*/
	
	return $event;
}  




// transforme un text d'event en un tableau de variable nomm� dudit event
/* (� tester... ) */
function texte_event2tab($texte){
	$tab_lignes=explode("\n", trim($texte)); //on separ le texte en lignes
	/*echo '<hr>$tab_lignes : <pre>';
	print_r($tab_lignes);
	echo "</pre>";*/
	foreach($tab_lignes as $ligne){ // regroupement des variable etant sur plisieur lignes (comme les description par exemple)
		if(trim($ligne)!=""){
			if (substr($ligne, 0,1)!=" "){ //si ce n'est pas une suite du texte de la pr�c�dente c'est que c'est une nouvelle variable
			
				//$variable=explode(":",$ligne); /*** ne marche pas avec "URL" ( � cause des ":" de "http://..." */
				//$tab[trim($variable[0])]=trim($variable[1]);

				$pos = stripos($ligne, ":");				
				$variable=trim(substr($ligne,0,$pos));
				$tab[$variable]=trim(substr($ligne,$pos+1));
			}else{ //sinon c'est que c'est une suite � concat�nner...
				$tab[$variable].=substr($ligne,1);
			}
		}
	}
	
	
	if (isset($tab['RRULE'])){ // parsage des repetions...
		$rrtab=explode(";",$tab['RRULE']);
		unset($tab['RRULE']);
		foreach($rrtab as $i){
			$j=explode("=",$i);
			$tab['RRULE'][$j[0]]=$j[1];
		}
		foreach($tab['RRULE'] as $r => $i)
			if(substr($r,0,2)=="BY")
				$tab['RRULE'][$r]=explode(",",$i);

	}
	
	
	//parsage des date de d�but et de fin de l'event
	foreach($tab as $label => $val){
		if (substr($label, 0,8)=="DTSTART;" || substr($label, 0,6)=="DTEND;"){
			$i=explode(";",$label);
			$tab[$i[0]]=$val;
			$j=explode("=",$i[1]);
			$tab[$j[0]]=$j[1];
			unset($tab[$label]);			
		}
	}
	
	
	/*echo '<hr>$tab : <pre>';
	print_r($tab);
	echo "</pre><hr>";*/
		
	return $tab;
}



/////////////////////////////////////////////////
//	Return Unix timestamp from ical date time format 
//
//	@param {string} $icalDate A Date in the format YYYYMMDD[T]HHMMSS[Z] or YYYYMMDD[T]HHMMSS
// $opt= null ou 
//	"j" pour ne donner que les jours...
//	"s" le nombre de secondes depuis le d�but du jour
/////////////////////////////////////////////////
function iCalDateToUnixTimestamp($icalDate,$opt=null){ 

	$icalDate = str_replace('T', '', $icalDate); 
	$icalDate = str_replace('Z', '', $icalDate); 

	$pattern  = '/([0-9]{4})';   // 1: YYYY
	$pattern .= '([0-9]{2})';    // 2: MM
	$pattern .= '([0-9]{2})';    // 3: DD
	$pattern .= '([0-9]{0,2})';  // 4: HH
	$pattern .= '([0-9]{0,2})';  // 5: MM
	$pattern .= '([0-9]{0,2})/'; // 6: SS
	preg_match($pattern, $icalDate, $date); 

	// Unix timestamp can't represent dates before 1970
	if ($date[1] <= 1970) {
		return false;
	} 
	/* Unix timestamps after 03:14:07 UTC 2038-01-19 might cause an overflow if 32 bit integers are used.*/
	if ($opt=="j"){
		$timestamp = mktime(0,  			// 4: HH 
							1,  			// 5: MM  (ajout de 100 secondes pour eviter
							40,  			// 6: SS  les confusion de jour autour de minuit)
							(int)$date[2],  // 2: MM 
							(int)$date[3],  // 3: DD 
							(int)$date[1]); // 1: YYYY 
	}elseif ($opt=="s"){
		$timestamp = ((int)$date[4])*60*60+ ((int)$date[5])*60 + ((int)$date[6]);
	}else{
		$timestamp = mktime((int)$date[4],  // 4: HH 
							(int)$date[5],  // 5: MM 
							(int)$date[6],  // 6: SS 
							(int)$date[2],  // 2: MM 
							(int)$date[3],  // 3: DD 
							(int)$date[1]); // 1: YYYY 
	}
	return  $timestamp;	
}

?>