<?php
$ret='<link rel="stylesheet" type="text/css" href="'.$theme.'monkey_date.css" media="all">';

$modif=($_POST["Monkey_Date"]!="creation");



/*echo '<hr>$_POST :<pre>';
print_r($_POST);
echo '</pre><hr>';*/




// si ce n'est pas une création, définir les variable par défaut
$cal_def="cal_1"; /* metre le premier calendrier local (il y en a forcément un sinon on ne doit pâs pouvoir ce retrouver ici ! ) ou permetre de configurer un calendrier local par defaut */

$event_text="(Nouvel évènement)";
$description="";

//autre info...
$lieu=""; /* permetre une option par defaut */
$url=""; /* permetre une option par defaut diférente... */ /* ou ne metre ? "$_SERVER['HTTP_HOST']" ? */

//repetition
$recur_INTERVAL=1;

//fin repet
$recur_COUNT=1;
$recur_FREQ="jamais";

//debut d'évènement par defaut
$start_unixtime = isset($_POST["date"])?$_POST["date"]:time(); /*arondir toute les 5 minutes ? */
/* $_POST["date"] met le debut de l'evenement à 00H00 du matin par defaut... il y à mieux à proposer comme l'heure courante de la saisi (comme tbird/ligntning) ou autre choses... */


//fin d'évènement par defaut (+1H)
$end_unixtime = $start_unixtime+3600;

//fin de répétion par defaut (+1J)
$until_unixtime = $start_unixtime+86400;





if ($modif){ //si ce n'est pas une création on récupère les données de l'event à modifier/cloner/enrichier
	/**************************/
			
			if ($metadonees_calendriers[$_POST['cal_event']]['type']=="local")$cal_def=$_POST['cal_event'];
	
			$event=texte_event2tab(capture_event($_POST["cal_event"], $_POST["id_event"])); //tableau de meta basic de calendrier !!!
			
			
			/*echo "<hr>event<br><pre>";
			print_r($event);
			echo "</pre><hr>";*/
			
			
			$event_text=netoyage_divers($event['SUMMARY'],"text"); //titre
			$description=isset($event['DESCRIPTION'])?netoyage_divers($event['DESCRIPTION'],"text"):""; //description, s'il y a...
			
			$start_unixtime = iCalDateToUnixTimestamp($event["DTSTART"]);
			$end_unixtime =   iCalDateToUnixTimestamp($event["DTEND"]);
			
			//repetition			
			if (isset($event['RRULE']['FREQ']))	$recur_FREQ=strtolower($event['RRULE']['FREQ']);	
			if (isset($event['RRULE']['INTERVAL']))$recur_INTERVAL=$event['RRULE']['INTERVAL'];
			
			//fin repet
			if (isset($event['RRULE']['COUNT']))$recur_COUNT=$event['RRULE']['COUNT'];
			if (isset($event['RRULE']['UNTIL']))$until_unixtime=iCalDateToUnixTimestamp($event['RRULE']['UNTIL'],"j");
			
			//autre info...
			$lieu=(isset($event['LOCATION'])?(htmlspecialchars_decode(netoyage_divers($event['LOCATION'],"text"))):"") ; /* balise openstretmap ? */
			$url=(isset($event['URL'])?(htmlspecialchars_decode($event['URL'])):"");

	/**************************/
}


//$wd = date("w", $timestamp);	
$jour_fr = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
$jour_ics = array("SU","MO","TU","WE","TH","FR","SA");
$jour_dm = ceil(date("j", $start_unixtime)/7);
$eniem = array(0,"premier","deuxième","troisième","quatrième","cinqième");
$mois_fr = array(0,"janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");



$ret.='<link rel="stylesheet" href="'.$theme.'base.css" type="text/css" />
	<link rel="stylesheet" href="'.$theme.'monkey_date.css" type="text/css" />

	<script type="text/javascript" src="JS/monkey_date.js"></script>

<!--<body style="overflow: auto;" onload="aff_repet();monkey_jour2();monkey_mois()">-->
	
	<form method="POST" action="" name="form" onload="aff_repet();monkey_jour2();monkey_mois()">
<div class="zonne" id="zonne"><div class="sous_zonne" id="sous_zonne_1">
		<input type="hidden" name="Monkey_Date_action" id="Monkey_Date_action" value="creation">
			Titre :
				<input required="required" name="titre" size="65" type="text" value="'.$event_text.'">
			<hr>';
			//affichage du calendrier d'origine si pas local
				if($modif) if($metadonees_calendriers[$_POST['cal_event']]['type']!="local")$ret.="Calendrier source : <i>".$metadonees_calendriers[$_POST['cal_event']]['nom']."</i><br>";

			
			$ret.='Calendrier : <select size="1" name="cal_cible" >';

			if($_SESSION['niv']<5){ //si droit d'ecriture global
				// choix entre tout les calendrier locaux
				foreach($metadonees_calendriers as $id => $tab)	if ($tab["type"]=="local") $ret.='<option value="'.$id.'" '.($cal_def==$id ?'selected="selected"':"").'>'.$tab['nom'].'</option>';
				
			}elseif ($_SESSION['niv']==5){ //sinon choix restrain par calendrier
				foreach($privilege as $id => $lvl) if ($lvl<5) $ret.='<option value="'.$id.'" '.($cal_def==$id ?'selected="selected"':"").'>'.$metadonees_calendriers[$id]['nom'].'</option>';
			}
			$ret.='</select>';
			
				if(isset($_POST['cal_event']))$ret.='<input type="hidden" name="cal_base" value="'.$_POST['cal_event'].'">';
				if($modif)$ret.='<input type="hidden" name="id_base" value="'.$_POST['id_event'].'">';
			
			
			$ret.='<hr>
			Date :<div style="display: inline-block;vertical-align:top;">
				<input name="journee" id="journee" type="checkbox" onchange="aff_even_journee()"';
					if(isset($event["TRANSP"]))if($event["TRANSP"] == "TRANSPARENT")$ret.="checked ";
				$ret.='/><label for="journee">Evenement sur la journée</label><br>
				du <input required="required" name="date1" size="10" class="mCalendarFR" value="'.date("d/m/Y", $start_unixtime).'" type="date"> à
					<select size="1" name="heure1" id="heure1">';
						
							for ($i=0; $i<=23; $i++) 
								$ret.='<option value="'.str_pad($i,2,"0",STR_PAD_LEFT).'"'.(str_pad($i,2,"0",STR_PAD_LEFT)==date("G", $start_unixtime)?' selected="selected"':"").'>'.
									str_pad($i,2,"0",STR_PAD_LEFT).
									'</option>';/***/
						
					$ret.='</select>heure et <select size="1" name="minute1" id="minute1">';
					
					$min=intval(date("i", $start_unixtime));
					for($i=0; $i<=55 ; $i+=5){
						$i0=sprintf("%02d", $i); // transforme "5" en "05"
						$sel="";
						if ($min>=$i && $min<$i+5) $sel=' selected="selected" ';
						$ret.='<option value="'.$i0.'"'.$sel.'>'.$i0.'</option>';
					}
					
					$ret.='</select>min.
				<br>
				au <input required="required" name="date2" size="10" class="mCalendarFR" value="'.date("d/m/Y", $end_unixtime).'" type="date"> à
					<select size="1" name="heure2" id="heure2">';
						
						for ($i=0; $i<=23; $i++)
							$ret.='<option value="'.str_pad($i,2,"0",STR_PAD_LEFT).'"'.(str_pad($i,2,"0",STR_PAD_LEFT)==date("G", $end_unixtime)?' selected="selected"':"").'>'.str_pad($i,2,"0",STR_PAD_LEFT).'</option>';/***/
						
					$ret.='</select>heure et <select size="1" name="minute2" id="minute2">';
					$min=intval(date("i", $end_unixtime));
					for($i=0; $i<=55 ; $i+=5){
						$i0=sprintf("%02d", $i); // transforme "5" en "05"
						$sel="";
						if ($min>=$i && $min<$i+5) $sel=' selected="selected" ';
						$ret.='<option value="'.$i0.'"'.$sel.'>'.$i0.'</option>';
					}
					$ret.='</select>min.';
			$ret.='</div><hr>
			Lieu :
				<input name="lieu" size="65" type="text" value="'.$lieu.'" />
			<hr>
			Description :<br><textarea rows="21" cols="62" name="description">'.$description.'</textarea><hr>
			Lien web :
				<input name="url" size="65" type="text" value="'.$url.'" />
			<hr>

</div><div class="sous_zonne" id="sous_zonne_2">

			Répétition : <span id="repet_form" style="visibility:hidden">Tout les <input step="1" min="1" size="4" value="'.$recur_INTERVAL.'" name="chaque" id="cr_nb_unit" type="number" onchange="monkey_unit(this)"></span>
				<select size="1" required="required" name="repet" id="repet" onchange="aff_repet()">';
					$ret.='<option value="jamais"  '.($recur_FREQ=="jamais" ?'selected="selected"':"").'>jamais</option>';
					$ret.='<option value="jour"    '.($recur_FREQ=="daily"  ?'selected="selected"':"").'>jour(s)</option>';
					$ret.='<option value="semaine" '.($recur_FREQ=="weekly" ?'selected="selected"':"").'>semaine(s)</option>';
					$ret.='<option value="mois"    '.($recur_FREQ=="monthly"?'selected="selected"':"").'>mois</option>';
					$ret.='<option value="an"      '.($recur_FREQ=="yearly" ?'selected="selected"':"").'>année(s)</option>';
				$ret.='</select>
			
				<span id="cr_jr_de_sm"><hr>Les : 
					<div style="display: inline-block;vertical-align:top;" id="cr_j_semaine">';
						for ($i=1;$i<=7;$i++) $ret.='<input value="'.$jour_ics[($i%7)].'" name="jours[]" id="jours_'.$i.'" type="checkbox"'.(date("w", $start_unixtime)==$i?'checked':"").' onchange="monkey_jour1('.$i.')"><label for="jours_'.$i.'">'.$jour_fr[($i%7)].'</label>'.($i==4?"<br>":"");
					$ret.='</div>
				<br></span>
				<span id="cr_aff_mois"><hr>
					<input name="choix_repet" value="xeme" id="cr_jour_dans_mois" onchange="disab_repet()" type="radio"><label for="cr_jour_dans_mois">Chaque</label>
						<select size="1" name="jour_a" id="jour_a"  disabled="disabled">';
							for($i=1;$i<=5;$i++)$ret.='<option value="'.$i.'"'.($i==$jour_dm?' selected="selected"':"").'>'.$eniem[$i].'</option>';
							$ret.='<option value="-1">dernier</option>
						</select>
						<select size="1" name="jour_b" id="jour_b"  disabled="disabled">';
							for ($i=1;$i<=7;$i++) $ret.='<option value="'.$jour_ics[($i%7)].'"'.(($i%7)==date("w", $start_unixtime)?' selected="selected"':"").'>'.$jour_fr[($i%7)].'</option>';
						$ret.='</select>
					<br>
					<input name="choix_repet" value="num" id="cr_jours_de_mois" onchange="disab_repet()" type="radio" checked><div style="display: inline-block;vertical-align:top;"><label for="cr_jours_de_mois">Tout les :</label><br>
						<table style="width: 440px;" id="cr_desac_js_ms" border="1"><tbody>';
							
							for($i=1;$i<=31;$i++){
								$if00=str_pad($i,2,"0",STR_PAD_LEFT);
								if (($i-1)%7==0) $ret.="<tr>";
									$ret.='<td><input value="'.$i.'" name="j[]" id="j'.$if00.'" type="checkbox"';
									$ret.=((isset($event['RRULE']['BYMONTHDAY']) &&
											in_array($i, $event['RRULE']['BYMONTHDAY']))||
											(date("d", $start_unixtime)==$if00))?
													' disabled="disabled" checked':"";
									$ret.=' onchange="monkey_jour2('.$i.')"><label for="j'.$if00.'">'.$i.'</label></td>';
								if ($i%7==0) echo "</tr>";
							}
							$ret.='
								<td colspan="4" rowspan="1"><input value="-1" name="j[]" id="j-1" type="checkbox"';
								$ret.=(isset($event['RRULE']['BYMONTHDAY']) && 
								in_array("-1", $event['RRULE']['BYMONTHDAY']))?
								'disabled="disabled" checked':"";
								$ret.='onchange="monkey_jour2(-1)"><label for="j-1">Dernier jour du mois</label></td>
							</tr>
						</tbody></table>
					</div>
				</span>
				<span id="cr_aff_annee">des mois de :<div style="margin-left: 21px;">
					<table style="width: 440px;" border="1"><tbody>';
						
						for($i=1;$i<=12;$i++){
							$if00=str_pad($i,2,"0",STR_PAD_LEFT);
							if (($i-1)%3==0) $ret.="<tr>";
							$ret.=($i<3?'<td style="width: 33%;">':'<td>');
							$ret.='<input value="'.$i.'" name="m[]" id="m'.$if00.'" type="checkbox"';
							$ret.=(
								date("m", $start_unixtime)==$i || (
								isset($event['RRULE']['BYMONTH']) && 
								in_array($if00, $event['RRULE']['BYMONTH'])
								)?'disabled="disabled" checked':"");
							$ret.=' onchange="monkey_mois('.$i.')"><label for="m'.$if00.'">'.$mois_fr[$i].'</label></td>';
							if ($i%3==0) $ret.="</tr>";
						}
						/******************************************************************/
					$ret.='</tbody></table>
				</div></span>
		<hr>
			<span id="repet_fin">
				<input type="radio" name="repet_fin" value="non" id="repet_fin_non"'.
					((isset($event["RRULE"]["UNTIL"]) || isset($event["RRULE"]["COUNT"]))?"":"checked").'/>
					<label for="repet_fin_non">Pas de date de fin.</label>
				<br>
				<input type="radio" name="repet_fin" value="nb" id="repet_fin_nb" '.((isset($event["RRULE"]["COUNT"]))?"checked":"").'/>
					<label for="repet_fin_nb">Créer</label>
					<input step="1" min="1" size="4" value="'.$recur_COUNT.'" name="fin_nb_unit" id="fin_nb_unit" type="number" onchange="monkey_unit(this)"> rendez-vous.
				<br>
				<input type="radio" name="repet_fin" value="date" id="repet_fin_date" '.((isset($event["RRULE"]["UNTIL"]))?"checked":"").'"/>
					<label for="repet_fin_date">Répéter j\'usqu\'au</label>
					<input name="date_fin" size="10" class="mCalendarFR" value="'.date("d/m/Y", $until_unixtime) .'" type="date">
			<hr></span>
</div></div>
	</form>';			
	
			if (!$modif){
				$ret.='<a href="#" class="bouton" onclick=\'document.getElementById("Monkey_Date_action").value="creer";document.form.submit();\'>Créer...</a> Creer un nouvel evenement.<br>';

				}else{
				//echo '<button class="bouton" onclick=\'document.getElementById("Monkey_Date_action").value="deriver";document.form.submit();\'>Dériver...</button> Créer un nouvel evenement dérivé (l\'anciène vertion ne sera toujour présente à l\'identique)<br>';
				$ret.='<a href="#" class="bouton" onclick=\'document.getElementById("Monkey_Date_action").value="deriver";document.form.submit();\'>Dériver...</a> Créer un nouvel evenement dérivé (l\'anciène vertion ne sera toujour présente à l\'identique)<br>';
				
				if ($metadonees_calendriers[$_POST['cal_event']]['type']=="local"){

					//echo '<button class="bouton" onclick=\'document.getElementById("Monkey_Date_action").value="modifier";document.form.submit();\'>Modifier...</button> Modifier l\'evenement.<br>';
					$ret.='<a href="#" class="bouton" onclick=\'document.getElementById("Monkey_Date_action").value="modifier";document.form.submit();\'>Modifier...</a> Modifier l\'evenement.<br>';


					//echo '<button class="bouton" onclick=\'document.getElementById("Monkey_Date_action").value="suprimer";document.form.submit();\'>suprimer...</button> Suprimer definitivrement l\'evenement.(coché les cases de part et d\'autre du bouton pour dévérouiller)<br>';
					$ret.='<a href="#" class="bouton" onclick=\'document.getElementById("Monkey_Date_action").value="suprimer";document.form.submit();\'>suprimer...</a> Suprimer definitivrement l\'evenement.(coché les cases de part et d\'autre du bouton pour dévérouiller)<br>';

				}
			}

		$ret.='<a href="'.$query.'" class="bouton" >Annuler...</a> On ne fait rien ! On oublie tout... Je ne voulais pas, c\'était une erreur. Pardon...

	
	<script type="text/javascript">
	// bien laisser à la fin !!!
	aff_repet();monkey_jour2();monkey_mois();aff_even_journee();
	</script>';

return $ret;
?>
	
	