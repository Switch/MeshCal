<?php
//MeshCal
// fonction de traitement des information au format icalendar (RFC 2445 et remplacentes...)




// Alimente les deux tableau principaux (en global) � partir d'un fichier ICS.
function IcsTab2Tabs($Ics,$cal,$cal_aff_debut,$cal_aff_fin){ // parcour d'un fichier ics
	global $liste_evens_description, $liste_evens_occurences;
	$bevent=false; // marquer pour savoir si on est dans un event
	//$tab_even=array();
	$famille="VEVENT";
	$UID="";
	
	$IcsTab = explode("\n", $Ics);
	
	
	/***/
	//while (($lcal = fgets($handle, 4096)) !== false) 
	/***/
	
	foreach($IcsTab as $lcal)if (trim($lcal)!=""){ //balayage du fichier ligne par ligne (si la ligne n'est pas vide !)


	if ( substr($lcal, 0 , 10) =="END:VEVENT"){ //fin de l'event : traitement...
			$id=$cal."|".$tab_even["VEVENT"]["UID"];
			if(event2tab_dates($id,$tab_even,$cal_aff_debut,$cal_aff_fin)) // si nouvelle entr� dans tableau jours par jour
					$liste_evens_description[$id]=$tab_even; // remplissage du tableau principal de description d'evenements
			$bevent=false;//et on r�initialise pour les autres event...
		}
		// gestion de VALARM	
		if(trim($lcal)=="BEGIN:VALARM")$famille="VALARM";
		if(trim($lcal)=="END:VALARM")$famille="VEVENT";
		
		if ($bevent){ //enregistrement de l'event...
			
			if(substr($lcal,0,1)==" "){ // les info peuvent rendre plusieur lignes. Les suivantes sont toujour pr�c�d� d'un espace.

			if (isset($tab_even[$famille][$rec[0]])) /* pour ignorer un truc bizard de googlecalendar genre : ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;... */
				$tab_even[$famille][$rec[0]].=trim($lcal);

			}else{
				$rec = array();
				$rec = explode(':',$lcal); // parsage

				// DESCRIPTION et SUMMARY peuvent contenir des ":" !!!
				if(isset($rec[1]) && trim($rec[1])!="VALARM")$tab_even[$famille][$rec[0]]=trim(substr($lcal,strlen ($rec[0])+1));
			}
		}
		if (substr($lcal, 0, 12) =="BEGIN:VEVENT"){ //debut d'event !!!
			//reinitialisation...
			$bevent=true;
			$famille="VEVENT";
			$tab_even[$famille]=array("CAL_ID" => $cal);
		}
	}
}








/* fonction obsolette mais qui peu reservir...
s'utilise avec une fichier comme suit :

	$handle = fopen($cal, "r");  //fichier ICAL (*.ics)
	if ($handle)ics2tabs($handle,$id_cal,$cal_aff_debut,$cal_aff_fin);  //si le calendrier s'ouvre alimenter les deux tableaux principaux...
	if (!feof($handle)) {
		echo "Erreur: fgets() a �chou�... <br>\n"; 
	}											
	fclose($handle);

*/
// Alimente les deux tableau principaux (en global) � partir d'un fichier ICS.
function ics2tabs($handle,$cal,$cal_aff_debut,$cal_aff_fin){ // parcour d'un fichier ics
	global $liste_evens_description, $liste_evens_occurences;
	$bevent=false; // marquer pour savoir si on est dans un event
	//$tab_even=array();
	$famille="VEVENT";
	$UID="";

	while (($lcal = fgets($handle, 4096)) !== false) if (trim($lcal)!=""){ //balayage du fichier ligne par ligne (si la ligne n'est pas vide !)
		if ( substr($lcal, 0 , 10) =="END:VEVENT"){ //fin de l'event : traitement...
			$id=$cal."|".$tab_even["VEVENT"]["UID"];
			if(event2tab_dates($id,$tab_even,$cal_aff_debut,$cal_aff_fin)) // si nouvelle entr� dans tableau jours par jour
					$liste_evens_description[$id]=$tab_even; // remplissage du tableau principal de description d'evenements
			$bevent=false;//et on r�initialise pour les autres event...
		}
		// gestion de VALARM	
		if(trim($lcal)=="BEGIN:VALARM")$famille="VALARM";
		if(trim($lcal)=="END:VALARM")$famille="VEVENT";
		
		if ($bevent){ //enregistrement de l'event...
			
			if(substr($lcal,0,1)==" "){ // les info peuvent rendre plusieur lignes. Les suivantes sont toujour pr�c�d� d'un espace.
				$tab_even[$famille][$rec[0]].=trim($lcal);
			}else{
				$rec = array();
				$rec = explode(':',$lcal); // parsage

				// DESCRIPTION et SUMMARY peuvent contenir des ":" !!!
				if(isset($rec[1]) && trim($rec[1])!="VALARM")$tab_even[$famille][$rec[0]]=trim(substr($lcal,strlen ($rec[0])+1));
			}
		}
		if (substr($lcal, 0, 12) =="BEGIN:VEVENT"){ //debut d'event !!!
			//reinitialisation...
			$bevent=true;
			$famille="VEVENT";
			$tab_even[$famille]=array("CAL_ID" => $cal);
		}
	}
}










//parcour de toute les ocurence d'un event dans une plage de dates � afficher
function event2tab_dates($even_id,$tab_even,$cal_aff_debut,$cal_aff_fin){
	// cr�er les entr�es dans $liste_evens_description
	//
	global $liste_evens_occurences;
	$event_dans_aff=false; // pointeur si l'event est present dans la plage � afficher
	$jour2s=60*60*24;	
	$even_rrule="";
	foreach($tab_even["VEVENT"] as $t => $c){
		if(substr($t,0,7)=="DTSTART")$even_debut_ical=$c;
		if(substr($t,0,5)=="DTEND")$even_fin_ical=$c;
		if($t=="RRULE")$even_rrule=$c;
	}
	$even_debut_jour=iCalDateToUnixTimestamp($even_debut_ical,"j");//date de debut de l'evenement premier
	$even_debut_s=iCalDateToUnixTimestamp($even_debut_ical,"s"); //heure de debut de l'evenement premier en secondes

	$even_fin_jour=iCalDateToUnixTimestamp($even_fin_ical,"j");//date de fin de l'evenement premier

	$even_debord_jour_s=$even_fin_jour-$even_debut_jour;
	$even_debord_jour=$even_debord_jour_s/$jour2s; //nombre de jour que debord l'evenement minimum : 0
	
	$even_repet_fin=$even_debut_jour; /* necessaire ??? */ //par defaut...	
	if ($even_rrule!=""){ //parsage de RRULE
		$rrules = array();
		$rrule_strings = explode(';',$even_rrule);
		$even_repet_fin=$cal_aff_fin; // si pas de fin, alors on prend la fin du calendrier
		foreach ($rrule_strings as $s) {
			list($k,$v) = explode('=', $s); /* ??? */
			$rrules[$k] = $v;
			if(substr($k,0,5)=="UNTIL")$even_repet_fin=iCalDateToUnixTimestamp($v,"j");//date de fin de r�p�tition en "YYYYMMDD[T]HHMMSS[Z]"... optionel, peut ne pas etre defini ou etre null. est normzalement dans RRULE
		}
	}
	
	if( $even_debut_jour <= $cal_aff_fin && $even_repet_fin+$even_debord_jour_s >= $cal_aff_debut ){// si la plage d'affichage chevauche bien les ocurences d'evens : c'est parti !!!
		$list_date=array(); //cr�ation/initialisation de la liste simple de dates des ocurences

		
		if($even_rrule != ""){// plusieur ocurences... traitement des r�p�titions.
			// evenement r�curent :
			$list_date=rrule2tab_debut($rrules, $even_debut_jour, $cal_aff_debut-$even_debord_jour_s, $cal_aff_fin);//LA fonction !!!
		}
		if($even_debut_jour>=$cal_aff_debut-$even_debord_jour_s)$list_date[]=$even_debut_jour;// traitement de la l'ocurence premi�re au besoin...*/

		if(!empty($list_date)){ //pr�paration du tableau jour par jour
			if($even_debord_jour>0){
				//Evenement etal� sur plusieur jour
				foreach ($list_date as $d){
					//remplisage de $liste_evens_occurences avec marqueur "debut" (debut d'evenement)
					if($d>=$cal_aff_debut){
						/** tester "TRANS+TRANSPARENT" pour savoir si l'evenement couvre la journ�e... */
						$liste_evens_occurences["jour_".$d]["heure_".$even_debut_s][$even_id]="debut"; /***/
						$event_dans_aff=true;
					}
					for($i=1; $i < $even_debord_jour; $i++){
						//remplisage de $liste_evens_occurences avec marqueur "milieu" (evenement en cour)
						$j=$d+$i*$jour2s;
						if($j>=$cal_aff_debut && $j<=$cal_aff_fin){
							$liste_evens_occurences["jour_".$j]["heure_".-1][$even_id]="milieu"; /***/
							$event_dans_aff=true;
						}
					}
					//remplisage de $liste_evens_occurences avec marqueur "fin" (fin d'evenement)
					$j=$d+$even_debord_jour*$jour2s;
					if($j<=$cal_aff_fin){
						/** tester "TRANS+TRANSPARENT" pour savoir si l'evenement couvre la journ�e... */
						$liste_evens_occurences["jour_".$j]["heure_".-1][$even_id]="fin"; /***/
						$event_dans_aff=true;
					}
				}
			}else{
				//Evenement compris dans une seul journ�e 
				foreach ($list_date as $d){
					//remplisage de $liste_evens_occurences avec marqueur "court"
					/** tester "TRANS+TRANSPARENT" pour savoir si l'evenement couvre la journ�e... */
					$liste_evens_occurences["jour_".$d]["heure_".$even_debut_s][$even_id]="court"; /***/
					$event_dans_aff=true;
				}
			}
		}
	}
	return $event_dans_aff;
} //fin de la fonction even2tab()













//       rrule2tab() converti un "RRULE" en une liste de dates contenu dans une plage calandaire fini.
//
//                 $rrule : la portion ICS de l'evenement � traiter
//                         $even_debut (optionel, tol�rance) : debut d'�venement defini dans l'ICS (format� en unixtime)
//                                       $cal_debut (optionel) : debut de la plage calandaire � afficher (format� en unixtime)
//                                                   $cal_fin (optionel) : debut de la plage calandaire � afficher (format� en unixtime)
function rrule2tab_debut($rrules, $even_debut, $cal_debut, $cal_fin){ 

/* Doit etre eradiqu� du code : */
/*** $anEvent */
	


	// tableau de sorti de resultat
	$tab = array();
	// Get Interval
	$interval = (isset($rrules['INTERVAL']) && $rrules['INTERVAL'] != '') ? $rrules['INTERVAL'] : 1;
	// Get Until
	if (isset($rrules['UNTIL'])){
		$until = iCalDateToUnixTimestamp($rrules['UNTIL']); /***/
	}else{// pas de fin, donc on en fabrique une pour l'ocasion...
		$until = $cal_fin ; /* plus la dur� en jour de l'evenement ? ... � v�rififier si c'est util mais dans le doute... */
	}

	// Decide how often to add events and do so
	switch ($rrules['FREQ']) {

	
	
		case 'DAILY':  /*** a v�rifier ***/
			// Simply add a new event each interval of days until UNTIL is reached
			$offset = "+$interval day";
			$recurring_timestamp = strtotime($offset, $even_debut);
			while ($recurring_timestamp <= $until && $recurring_timestamp <= $cal_fin) {
				// Add event
				if($recurring_timestamp>=$cal_debut)$tab[]=$recurring_timestamp;
				// Move forward
				$recurring_timestamp = strtotime($offset,$recurring_timestamp);
			}
		break;

	

		case 'WEEKLY':  /*** a v�rifier ***/
			// Create offset
			$offset = "+$interval week";
			// Build list of days of week to add events
			$weekdays = array('SU','MO','TU','WE','TH','FR','SA');
			$bydays = (isset($rrules['BYDAY']) && $rrules['BYDAY'] != '') ? explode(',', $rrules['BYDAY']) : array('SU','MO','TU','WE','TH','FR','SA');/***** ne prend pas les valeur multiple !!! (si plusieur jour) */
			// Get timestamp of first day of start week
			$week_recurring_timestamp = (date('w', $even_debut) == 0) ? $even_debut : strtotime('last Sunday '.date('H:i:s',$even_debut), $even_debut);
			// Step through weeks
			while ($week_recurring_timestamp <= $until && $week_recurring_timestamp <= $cal_fin) {
				// Add events for bydays
				$day_recurring_timestamp = $week_recurring_timestamp;
				foreach ($weekdays as $day) {
					// Check if day should be added
					if (in_array($day, $bydays) && $day_recurring_timestamp > $even_debut && $day_recurring_timestamp <= $until) {
						// Add event to day
						if($day_recurring_timestamp>=$cal_debut)$tab[]=$day_recurring_timestamp;
					}
					// Move forward a day
					$day_recurring_timestamp = strtotime('+1 day',$day_recurring_timestamp);
				}
				// Move forward $interaval weeks
				$week_recurring_timestamp = strtotime($offset,$week_recurring_timestamp);
			}
		break;



		case 'MONTHLY':  
			// Create offset
			$offset = "+$interval month";
			/*$recurring_timestamp = strtotime($offset, $even_debut); (moa) */
			$recurring_timestamp = $even_debut;
			
			
			if (isset($rrules['BYMONTHDAY']) && $rrules['BYMONTHDAY'] != '') {  /*** a v�rifier ***/ /***** ne prend pas les valeur multiple !!! (si plusieur jour) */
			  // Deal with BYMONTHDAY
			  while ($recurring_timestamp <= $until && $recurring_timestamp <= $cal_fin) {
				// Add event
				if($recurring_timestamp>=$cal_debut)$tab[]=$recurring_timestamp;
				// Move forward
				$recurring_timestamp = strtotime($offset,$recurring_timestamp);
			  }
			}elseif (isset($rrules['BYDAY']) && $rrules['BYDAY'] != '') {  /*** a v�rifier ***/ /***** ne prend pas les valeur multiple !!! (si plusieur jour) */
				$start_time = date('His',$even_debut);
				// Deal with BYDAY
				$day_number = substr($rrules['BYDAY'], 0, 1);/***** ne prend pas les valeur multiple !!! (si plusieur mois) */
				$week_day = substr($rrules['BYDAY'], 1);/***** ne prend pas les valeur multiple !!! (si plusieur mois) */
				$day_cardinals = array(1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth');
				$weekdays = array('SU' => 'sunday','MO' => 'monday','TU' => 'tuesday','WE' => 'wednesday','TH' => 'thursday','FR' => 'friday','SA' => 'saturday');
				while ($recurring_timestamp <= $until && $recurring_timestamp <= $cal_fin) {
					$event_start_desc = "{$day_cardinals[$day_number]} {$weekdays[$week_day]} of ".date('F',$recurring_timestamp)." ".date('Y',$recurring_timestamp)." ".date('H:i:s',$recurring_timestamp);
					$event_start_timestamp = strtotime($event_start_desc);
					if ($event_start_timestamp > $even_debut
							&& $event_start_timestamp < $until
							&& $recurring_timestamp>=$cal_debut)
									$tab[]=$event_start_timestamp;
					// Move forward
					$recurring_timestamp = strtotime($offset,$recurring_timestamp);
				}
			}



		break;
		case 'YEARLY': /*** controletr que ca le fait s'il y � plusieur mois !!! */   /*** a v�rifier ***/
			// Create offset
			$offset = "+$interval year";
			/*$recurring_timestamp = strtotime($offset, $even_debut); (moa) sinon ca peu oublier le premier event*/
			$recurring_timestamp = $even_debut;
			
			
			
			$month_names = array(1=>"January", 2=>"Februrary", 3=>"March", 4=>"April", 5=>"May", 6=>"June", 7=>"July", 8=>"August", 9=>"September", 10=>"October", 11=>"November", 12=>"December");
			$mois=explode(',',$rrules['BYMONTH']);

			// HACK: Exchange doesn't set a correct UNTIL for yearly events, so just go 2 years out
			$until = strtotime('+2 year',$even_debut);
			// Check if BYDAY rule exists
			if (isset($rrules['BYDAY']) && $rrules['BYDAY'] != '') {  /*** $rrules['BYDAY'] sera forc�ment un tableau ! a revoir... ***/
			  $start_time = date('His',$even_debut);
			  // Deal with BYDAY
			  
			  /*** a revoir compl�tement !!! pour choisir les dernier jour du mois, il faut des valeur negative ! (donc sur 2 caractaire)*/
			  $day_number = substr($rrules['BYDAY'], 0, 1);/***** ne prend pas les valeur multiple !!! (si plusieur jour) */
			  $month_day = substr($rrules['BYDAY'], 1);/***** ne prend pas les valeur multiple !!! (si plusieur jour) */
			  /*** a revoir ... */
			  
			  
			  $day_cardinals = array(1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth');
			  $weekdays = array('SU' => 'sunday','MO' => 'monday','TU' => 'tuesday','WE' => 'wednesday','TH' => 'thursday','FR' => 'friday','SA' => 'saturday');
			  while ($recurring_timestamp <= $until && $recurring_timestamp <= $cal_fin) {
				$event_start_desc = "{$day_cardinals[$day_number]} {$weekdays[$month_day]} of {$month_names[$rrules['BYMONTH']]} ".date('Y',$recurring_timestamp)." ".date('H:i:s',$recurring_timestamp); /***** ne prend pas les valeur multiple !!! (si plusieur mois) */
				$event_start_timestamp = strtotime($event_start_desc);
				if ($event_start_timestamp > $even_debut && $event_start_timestamp < $until) {
				  /*$anEvent['DTSTART'] = date('Ymd\T',$event_start_timestamp).$start_time; /***/
				  /*$events[] = $anEvent; /***/
				  if($recurring_timestamp>=$cal_debut)$tab[]=$event_start_timestamp;
				}
				// Move forward
				$recurring_timestamp = strtotime($offset,$recurring_timestamp);
			  }
			} else {  // verifier !
				$jour_m=(isset($rrules['BYMONTHDAY'])?explode(',',$rrules['BYMONTHDAY']):array(0=>date('d',$even_debut)));
				//print_r($jour_m);
				while ($recurring_timestamp <= $until && $recurring_timestamp <= $cal_fin) {
					// pour tout les jours et les mois concern�
					foreach($jour_m as $j)foreach($mois as $m){
						$event_start_desc = "$j {$month_names[$m]} ".date('Y',$recurring_timestamp)." ".date('H:i:s',$recurring_timestamp);
						//echo "[".$event_start_desc."]";
						$event_start_timestamp = strtotime($event_start_desc);
						if ($event_start_timestamp > $even_debut && $event_start_timestamp < $until) {
							if($recurring_timestamp>=$cal_debut)$tab[]=$event_start_timestamp;
						}
					}
					
					// Move forward
					$recurring_timestamp = strtotime($offset,$recurring_timestamp);
				}
			}
		break;
	}
	
	return $tab;
}



?>