<?php
$langue="fr"; /** a rendre parametrable avec une variable de session */

//remplissage du tableau de message selon la langue selection�
	//Message par d�faut en francais (en redondance avec la routine suivante en cas de traduction incomplette)
$csv = fopen($bases.'langues/'.$langue.'/dialogues.csv', 'rb'); // fichier de langue par d�faut
while (($i = fgetcsv($csv, 1000, ";")) !== FALSE) {
		$mess[$langue][$i[0]]=utf8_encode($i[1]); // table de la langue par d�faut
}
fclose($csv);
$messages=$mess[$langue];/*** remplacer tout les "$message" par des "$mess[$_SESSION['lng']]" */

function charge_langue($lg){//chargement d'une langue supl�mentaire
	global $mess, $langue, $bases;
	$mess[$lg]=$mess[$langue]; // pr�chargement des message en francais (qui seront ensuite ecras�s s'il existe une corespondance dans la langue cible)
	
	$csv = fopen($bases.'langues/'.$lg."/dialogues.csv", 'rb');
	while (($i = fgetcsv($csv, 1000, ";")) !== FALSE) {
		$mess[$lg][$i[0]]=utf8_encode($i[1]);
	}
	fclose($csv);
}


if (isset($_SESSION) && $_SESSION['lng']!=$langue  && $_SESSION['lng']!="")charge_langue($_SESSION['lng']); //pr�chargement de la langue de l'utilisateur si ce n'est pas celle par d�faut


function message($num,$tab_1d=array(),$lg=""){ /**/
	global $mess, $langue; /***/
	//echo "(message : ".$num." | langue : ".$lg.")";
	if (isset($_SESSION['lng'])){$slng=$_SESSION['lng'];}else{$slng=$langue;};
	if ($lg=="") $lg=(($slng!="")?$slng:$langue); //pour l'interface de connecsion...
	if(!isset($mess[$lg])) charge_langue($lg); // chrgement d'une langue supl�mentaire si ce n'est deja fait.
	$ret=$mess[$lg][$num];
	$ret= str_replace('\n',"\r\n",$ret);
	foreach($tab_1d as $i => $j) $ret= preg_replace("[\[#".$i."#\]]",$j,$ret);
	return $ret;
}

?>