<?php // routine d'inscription de nouveau utilisateur dans la base
/*echo "<pre>";
print_r($_POST);
echo "</pre>";*/

function charge_base_utilisateurs(){
	global $bases;
	$tab=array();
	$csv = fopen($bases.'/utilisateurs.csv', 'rb');
	while (($i = fgetcsv($csv, 1000, "|")) !== FALSE){
		$tab[]=array('log'=>$i[1], 'cou'=>$i[2]);
	}
	fclose($csv);
	return $tab;
}


function ajout_dans_fic($ligne,$fichier){/* A généraliser !!! */
	if(!file_exists($fichier)){
		//creation du fichier encore inexistant.
		$old = umask(0);
		$csv = fopen($fichier,"w");
			fwrite($csv, $ligne);
		fclose($csv);
		chmod($fichier, 0766);
		umask($old);
	}else{
		//ajout d"une ligne
		$csv = fopen($fichier,"a");
			fwrite($csv,"\r\n".$ligne);
		fclose($csv);
	}
}


// Envoi des info de coinexion (sans traces dans la base)

function courriel_info_connexion($log,$password,$adrcourriel,$langue){//couriel d'information de connexion
	global $courriel;
	
	$dest  = $adrcourriel;
	$titre = message(104,array(),$langue); /***/
	$corp = message(105, /***/
		array(
			"nom" => $log ,
			"log"=> $_SESSION['log'],
			"pw" => $password,
			"cnx"=> $_SERVER["HTTP_REFERER"] 
		),
		$langue)."<br/>";// r�cuperation du message selon la langue du destinataire
	mail($dest, $titre, $corp,"From:".$courriel."\n");

	$inscrit=$log;
	mail( //courier de confirmation � l'inscripteur
		$_SESSION['cou'], message(104)." - ".$inscrit,
		message(97,array( /***/
			"nom" =>$inscrit,
			"log"=>$_SESSION['log'],
			"cou"=>$adrcourriel
		)),
		"Bcc:".$courriel."\n"."From:".$courriel."\n"
	);
}

function initial($txt){ /***/
	$ret="";
	$txt = preg_replace('/([^a-z_])/i', '_', $txt);
	$explode=explode('_', $txt);
	foreach($explode as $i)$ret.=$i[0];
	return $ret;
}

function test_identifiant_existant($login){
	global $img_utilisateurs; /***/
	foreach($img_utilisateurs as $tab_1d){
		if ($tab_1d["log"]==$login){
			return true;
			break;
		}
	}
	return false;
}

function verif_courriel($courriel){
// Auteur : bobocop (arobase) bobocop (point) cz
// Traduction des commentaires par mathieu http://www.developpez.net/forums/u20527/mathieu/

// Le code suivant est la version du 2 mai 2005 qui respecte les RFC 2822 et 1035
// http://www.faqs.org/rfcs/rfc2822.html
// http://www.faqs.org/rfcs/rfc1035.html

	$atom   = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';		// caract�res autoris�s avant l'arobase
	$domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)';	// caract�res autoris�s apr�s l'arobase (nom de domaine)
                               
	$regex = '/^' . $atom . '+' .   				// Une ou plusieurs fois les caract�res autoris�s avant l'arobase
			'(\.' . $atom . '+)*' .         		// Suivis par z�ro point ou plus
													// s�par�s par des caract�res autoris�s avant l'arobase
			'@' .                           		// Suivis d'un arobase
			'(' . $domain . '{1,63}\.)+' .  		// Suivis par 1 � 63 caract�res autoris�s pour le nom de domaine
													// s�par�s par des points
			$domain . '{2,63}$/i';          		// Suivi de 2 � 63 caract�res autoris�s pour le nom de domaine

// test de l'adresse e-mail
	return !(preg_match($regex, $courriel));
}

function test_courriel_existant($courriel){
	global $img_utilisateurs; /***/
	foreach($img_utilisateurs as $tab_1d){
		if ($tab_1d["cou"]==$courriel){
			return true;
			break;
		}
	}
	return false;
}

function list_calendriers_html_select($cour=NULL){ /*liste de calendrier*/
	global $metadonees_calendriers;/***/

	/*echo "<pre>";
	print_r ($metadonees_calendriers);
	echo "</pre>";*/

	$ret="";
	foreach($metadonees_calendriers as $i => $j)if($j["type"]=="local"){
		$ret.=('<option '.($cour==$i?"selected ":"").'value="'.$i.'">'.$j["nom"].'</option>'."\n");/***/
	}
	return $ret;
}

function compil_liste_erreur($csv){ /*** integrer le tableau g�n�ral d'erreur � la place */
	$tab = array();
	$tab=explode(",",trim($csv,","));
	$ret="";
	//$ret=$csv."<br/>";/***/
	foreach($tab as $i){
		if ($i!="96") $ret.= message($i)."<br/>";
	}
	return $ret;
}

function nouveau_mot_de_pass_base(){ /* confidentiel ! ne pas difuser en open source...*/
	$pw='';$p1='';$p2='';$p3='';
	$c = 'bcdfghjkmnprstvwzBCDFGHJKLMNPRSTVWZbcdfghjkmnprstvwz';
	$v = 'aeiouyAEUYaeiouaeiou';
	$l = $c.$v.$v;
	$s ='+-*_&?+-';

	for($i=0;$i < 2; $i++){
		$p1 .= $c[rand(1, strlen($c))-1];
		if (rand(0,2)==1){$p1 .= $l[rand(1, strlen($l))-1];}
		$p1 .= $v[rand(1, strlen($v))-1];
	}
	$p2 = rand(10,999);
	$p3=$s[rand(1, strlen($s))-1];

	switch (rand(1,8)) {
		case 1: $pw = rtrim($p1.$p2.$p3);break;
		case 2: $pw = rtrim($p1.$p3.$p2);break;
		case 3: $pw = rtrim($p2.$p1.$p3);break;
		case 4: $pw = rtrim($p2.$p3.$p1);break;
		case 5: $pw = rtrim($p3.$p1.$p2);break;
		case 6: $pw = rtrim($p3.$p2.$p1);break;
		case 7: $pw = rtrim($p1.$p3.$p2);break;
		case 8: $pw = rtrim($p2.$p3.$p1);break;
	}
	return $pw;
}



//// traitement... ////

$nb_lignes_interface=1;
while (isset($_POST[($nb_lignes_interface+1)."identifiant"]))$nb_lignes_interface++;
//$liste_aff=list_aff2();/***/ //$metadonees_calendriers
$liste_lang=array("fr"); /***/ //$liste_lang=list_lang();
$i=0;
$table_entrees = array();
$table_inscrits = array();
$erreur = array();
$tab_1d= array();

if(isset($_POST["1identifiant"])){// aquisition des informations...
	for ($nb=1; $nb<=$nb_lignes_interface; $nb++){ //remplissage depuis formulaire
		$tab_1d[0]=trim($_POST[$nb.'identifiant']); /* identifiant */
		//$tab_1d[1]=trim($_POST[$nb.'nom']);			/***/
		$tab_1d[1]=trim($_POST[$nb.'courriel']);
		$tab_1d[2]=trim($_POST[$nb.'calendrier']);	/***/
		$tab_1d["l"]=trim($_POST[$nb.'langue']);
		$tab_1d["a"]=(strval($_POST[$nb.'calendrier'])!="0")?$_POST[$nb.'calendrier']:""; /* calendrier */
		$tab_1d[3]=trim($_POST[$nb.'infodiv']);
		$table_entrees[]=$tab_1d;
	}

	if (isset($_FILES['fichier']['tmp_name'])){// recuperation des info de fichier "csv" uploadé...
		if($_FILES['fichier']['tmp_name']!="")if($file = fopen($_FILES['fichier']['tmp_name'],"r")){//test présence fichier
			/* que deviennent les fichier 'tmp_name' aprés ??? */
			for($i=1;$i<=7;$i++){//permet d'ajouter 6 ligne de commentaire avent la liste proprement dite.
				$entete=fgets($file);
				
				$separateur_csv="";
				$position_sep1=0;
				$position_sep2=0;
				
				$position_sep1=strpos($entete, "]");
				$position_sep2=strpos($entete, "[",$position_sep1+1);
				if ($position_sep2-$position_sep1==2) $separateur_csv=substr($entete,$position_sep1+1,1);
				if ($position_sep2-$position_sep1==4) $separateur_csv=substr($entete,$position_sep1+2,1);
				if ($separateur_csv!="")break;
			}

			
			if ($separateur_csv!=""){
				while(! feof($file)){
					$tab_1d=(fgetcsv($file,0,$separateur_csv));
					if (count($tab_1d)>1){
						foreach($tab_1d as &$j){$j=trim($j,"\"' ");}unset($j);
					//foreach($tab_1d as $k => $j){$tab_1d[$k]=trim($tab_1d[$k],"\"' ");}
						if (isset($tab_1d[0])) if (count($tab_1d)>1) if(!($tab_1d[0]==""&&$tab_1d[1]=="")){//test ligne vide &&$tab_1d[1]==""
							$tab_1d["a"]="";$tab_1d["l"]="";
							$table_entrees[]=$tab_1d;
						}
					}
				}
			}else print (message(101)); // message rouge "Fichier invalide, utiliser le gabarit..."
		fclose($file);}
	}
	$tab_1d= array();
}

if(count($table_entrees)>0){//chargement de la base en memoire... control de validit� des entr�es
	$img_utilisateurs = array();
	$img_utilisateurs = charge_base_utilisateurs();
	foreach($table_entrees as &$tab_1d){//verification conformité et traitement
		$tab_1d["e"]="";
		if($tab_1d[0]=="")$tab_1d["e"].="77,"; // pas d'identifiant renseigné
		if ($tab_1d["e"]==""){
			$log=$tab_1d[0]; //nouvel_identifiant($tab_1d[0],$tab_1d[1]); /***/
		}else{}
		if ($tab_1d[1]!=""){
			if(verif_courriel($tab_1d[1]))$tab_1d["e"].="92,"; //Adresse courriel non valide
			if(test_courriel_existant($tab_1d[1]))$tab_1d["e"].="93,";// Adresse deja pr�sente dans la base
		}else $tab_1d["e"].="94,"; //Pas d'adresse courriel renseigné
			
		//if($tab_1d[2]=="")$tab_1d["e"].="95,"; //Pas de nom d'calendrier renseign�
		
		if ($tab_1d["e"]==""){// g�n�ration de la ligne � ajouter dans la base
			
			$ligne=$log."|";
			$passw=nouveau_mot_de_pass_base();
			$ligne.=hash("sha256",$passw)."|";/* passer en SHA1... plus tard */
			$ligne.=$tab_1d[1]."|";
			$ligne.=("5|");	//niveau d'autorisation par defaut
			
			if (in_array($tab_1d["l"],$liste_lang)){//traitement info langue
				$ligne.=$tab_1d["l"]."|";
			}else{
				$tab_1d["e"].="96,"; //Pas de langue deffini
				$tab_1d["l"]="";
			}
			$ligne.=date("d/m/y")."|";
			
			for($j=2;$j<=count($tab_1d)-4;$j++)
				$ligne.=$tab_1d[$j]."|";//insertion d'éventuel autres information...
			if (in_array($tab_1d["a"],$metadonees_calendriers)){
				$ligne.=$tab_1d["a"];}else $tab_1d["a"]=""; /***/ //control si le nom du calendrier n'a pas �t� forc� de facon inapropri�.
		}
		echo "(erreur : ".$tab_1d["e"].")";
		if($tab_1d["e"]==""){ //traitement des entrés valides
			ajout_dans_fic($ligne,$bases.'/utilisateurs.csv'); //ajout de la ligne à la base général d'utilisateur
			//function courriel_info_conexion($login,$password,$adrcourriel,$langue)
			// envoi un courriel spécifique d'information de connexion (dans la langue du nouvel inscrit)
			courriel_info_connexion($log,$passw,$tab_1d[1],$tab_1d["l"]); /*** lange prend l'adresse courriel !!! */
			
			//message standard(vert) de confirmation pour inscripteur
			$information[]=array(message(102,array("nom" => $log."(".$tab_1d[2].")")),"vert");/** creer popup de message */
			if($tab_1d["a"]!=""){
				inscr_dans_aff($log,$tab_1d[2],$tab_1d["a"],$tab_1d["l"]); //inscription du nouvel utilisateur dans le calendrier demand� 
			}
			
			$img_utilisateurs[]=array('log'=>$log, 'cou'=>$tab_1d[1], 'soc'=>$tab_1d[2]);
		}else if($tab_1d["e"]!="77,94,") $table_inscrits[]=$tab_1d;//si entrée incorect (et non vide) : rebelote...
	}
}
$tab_1d= array();// cr�ation d'une ligne vide pour permetre une entr�e manuelle.
for($i=0;$i<5;$i++) $tab_1d[]="";
$tab_1d["e"]="";
$tab_1d["l"]="";
$tab_1d["a"]="";
$table_inscrits[]=$tab_1d;







 //// Génération du tableau HTML d'entrées et de correction ////
//--------------------------------------------------------------------------------

$nb_inscrit=count($table_inscrits);
// script de remplissage auto de "calendrier"
$ret1=<<<EOT
<script type="text/javascript">
<!--
function aff_presel(){
	var moa=document.getElementById("calendriers").value;
	if(moa!=""){
		for (i=1;i<=${nb_inscrit};i++)document.getElementById(i+"calendrier").value=moa;
		if(moa==0) document.getElementById("calendriers").value="";
	}
}
-->
</script>
<form method="post" action="." enctype="multipart/form-data" >
EOT;

$ret1.='<input name="admin" value="inscription_nouvel_utilisateur" type="hidden">';





// ligne de titre
$ret1.="
<input type='hidden' name='nb_inscrit' value='${nb_inscrit}'>
	<table>
		<tr>
			<th>&nbsp;</th>";
			$ret1.="<th>".message(79)."</th>"; //identifiant
			//$ret1.="<th>".message(82)."</th>"; //Société
			$ret1.="<th>".message(83)."</th>"; //Courriel
			$ret1.="<th>".message(84)."</th>"; //Langue
			$ret1.="<th><noscript>".message(78)."</noscript>"; //calendrier
			$ret1.='<select id="calendriers" onChange="aff_presel()" onkeyup="aff_presel()" style="display:none">
						<option value="" selected="selected">'.message(78).' :</option>
						<option value="0">('.message(86).')</option>';
$ret1.=list_calendriers_html_select();

$ret1.=<<<EOT
				</select><script type="text/javascript">document.getElementById("calendriers").style.display = 'block';</script>
			</th>
EOT;

			$ret1.="<th>".message(87)."</th>
		</tr>";

		$nb=0;
// ligne de formulaire préremplis...
foreach ($table_inscrits as $tab_1d){
	$nb++;
	$ret1.="
		<tr>
			<td>${nb}</td>
			<td><input type=text name='${nb}identifiant'  value='${tab_1d[0]}' size='12' /></td>".
			//<td><input type=text name='${nb}nom' value='${tab_1d[1]}' size='12' /></td>
			//"<td><input type=text name='${nb}calendrier'  value='${tab_1d[3]}' size='12' /></td>
			"<td><input type=text name='${nb}courriel'  value='${tab_1d[1]}' size='25' /></td>
			<td>
				<select name='${nb}langue' id='${nb}langue'  style='width:100%;text-align:center'>";

	$l=($tab_1d["l"]==""?$langue:$tab_1d["l"]); //si pas de langue déffini alors préremplissage de proposition avec langue par défaut.
	foreach($liste_lang as $j){
		$ret1.=('<option '.($l==$j?"selected ":"").'value="'.$j.'">'.$j.'</option>'."\n");
	}

	$ret1.="
					</select>
				</td>
				<td>
					<select name='${nb}calendrier' id='${nb}calendrier'>
						<option value='0'></option>";

	$ret1.=list_calendriers_html_select($tab_1d["a"]);

	$ret1.="
					</select>
				</td>
				<td><input type=text name='${nb}infodiv'  value='${tab_1d[3]}' size='20' /></td>
			</tr>";

	if ($tab_1d["e"]!=""){// affichage d'erreur...
		$ret1.="<tr><td colspan='6'>";
		$ret1.=compil_liste_erreur($tab_1d["e"]);
		$ret1.="</td></tr>";
		/* emplacement pour surchoix contextuel...*/
	}
}

// fin de tableau et bouton de soumition...
//$moa=file_exists("public/charrette/langues/".$_SESSION['lng']."/gabarit_inscriptions.csv")?$_SESSION['lng']:$langue;
$ret1.="
	</table>
	".
	//"<a href='public/charrette/langues/".$moa."/gabarit_inscriptions.csv'>".message(100)."(csv)</a> <input name='fichier' type='file' accept='text/csv'/>
	"<a href='gabarit_inscriptions.csv'>".message(100)."(csv)</a> <input name='fichier' type='file' accept='text/csv'/>
	".
"<br/>
	<input type='submit' name='Submit' value='lancer inscription...' />
</form>";


return $ret1;
?>