function aff_sub(a, b, c, d, e){
	document.getElementById("repet_form").style.visibility=a;
	document.getElementById("cr_jr_de_sm").style.display=b;
	document.getElementById("cr_aff_mois").style.display=c;
	document.getElementById("cr_aff_annee").style.display=d;
	document.getElementById("repet_fin").style.display=e;
}

function aff_repet(){
	var selectElmt = document.getElementById("repet");
	select=( selectElmt.options[selectElmt.selectedIndex].value);
	if(select=="jamais") aff_sub('hidden',  'none',  'none',  'none',  'none' );
	if(select=="jour")   aff_sub('visible', 'none',  'none',  'none',  'block');
	if(select=="semaine")aff_sub('visible', 'block', 'none',  'none',  'block');
	if(select=="mois")   aff_sub('visible', 'none',  'block', 'none',  'block');
	if(select=="an")     aff_sub('visible', 'none',  'block', 'block', 'block');
}

function aff_even_journee(){
	moa=document.getElementById('journee').checked;
	document.getElementById("heure1").disabled = moa;
	document.getElementById("minute1").disabled = moa;		
	document.getElementById("heure2").disabled = moa;		
	document.getElementById("minute2").disabled = moa;		
}

function disab_repet(){
	document.getElementById("jour_a").disabled = true;			
	document.getElementById("jour_b").disabled = true;			
	for (i = 0; i < document.getElementById("cr_desac_js_ms").getElementsByTagName("input").length; i++){
		document.getElementById("cr_desac_js_ms").getElementsByTagName("input")[i].disabled = true;
	}
	for (i = 0; i < document.getElementById("cr_aff_annee").getElementsByTagName("input").length; i++){
		document.getElementById("cr_aff_annee").getElementsByTagName("input")[i].disabled = true;
	}

	if(document.getElementById('cr_jour_dans_mois').checked){
		document.getElementById("jour_a").disabled = false;			
		document.getElementById("jour_b").disabled = false;			
		for (i = 0; i < document.getElementById("cr_aff_annee").getElementsByTagName("input").length; i++){
			document.getElementById("cr_aff_annee").getElementsByTagName("input")[i].disabled = false;
		}			
	}
	if(document.getElementById('cr_jours_de_mois').checked){
					for (i = 0; i < document.getElementById("cr_desac_js_ms").getElementsByTagName("input").length; i++){
			document.getElementById("cr_desac_js_ms").getElementsByTagName("input")[i].disabled = false;
		}			
		for (i = 0; i < document.getElementById("cr_aff_annee").getElementsByTagName("input").length; i++){
			document.getElementById("cr_aff_annee").getElementsByTagName("input")[i].disabled = false;
		}
	}
}


function monkey_unit(that){
	unit=parseInt(that.value);
	if (unit<1||isNaN(unit))unit=1;
	that.value=unit;
}

function zerozero(i){
	var reg=/^([0-9]{1})$/;
	var tempo=i.toString();
	return tempo.replace(reg,'0$1')
}

function monkey_jour1(j){
	for (i=1;i<=7;i++) document.getElementById("jours_"+i).disabled=false;
	compteur=0;
	for (i=1;i<=7;i++) if (document.getElementById("jours_"+i).checked) compteur++;
	//if (compteur<2) for (i=1;i<=7;i++) document.getElementById("jours_"+i).disabled=(document.getElementById("jours_"+i).checked);
	if (compteur<1) document.getElementById("jours_"+j).checked=true;
}

function monkey_jour2(j){
	for (i=-1;i<=31;i++) if(i!=0) document.getElementById("j"+zerozero(i)).disabled=false;
	compteur=0;
	for (i=-1;i<=31;i++) if(i!=0) if (document.getElementById("j"+zerozero(i)).checked) compteur++;
	//if (compteur<2) for (i=-1;i<=31;i++) if(i!=0) document.getElementById("j"+zerozero(i)).disabled=(document.getElementById("j"+zerozero(i)).checked);
	if (compteur<1) document.getElementById("j"+zerozero(j)).checked=true;
}

function monkey_mois(m){
	for (i=1;i<=12;i++) document.getElementById("m"+zerozero(i)).disabled=false;
	compteur=0;
	for (i=1;i<=12;i++) if (document.getElementById("m"+zerozero(i)).checked) compteur++;
	//if (compteur<2) for (i=1;i<=12;i++) document.getElementById("m"+zerozero(i)).disabled=(document.getElementById("m"+zerozero(i)).checked);
	if (compteur<1) document.getElementById("m"+zerozero(m)).checked=true;
}